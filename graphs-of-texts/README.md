# Graphs of texts

Program that creates a graph from the words of a text. The purpose of it is do some experiments with the graph. Some analyzes can be: How many vertices does the graph have? And edges? Is the graph connected? How many components does the graph have?

This program was made for MAC0323 - Algoritmos e Estruturas de Dados II.


## How to execute
To compile and execute this program, follow the commands below:

1. make
2. ./ep2 #name_of_text_file# #min_words#

OBS.: In the #name_of_text_file#, place the .txt file containing the desejed text. Besides, in #min_words#, place the mininum length for the words of text that will be in the graph.
