#include <iostream>
#include <sstream>
#include <fstream>
#include <cstdlib>
#include <time.h>
#include "Graph.hpp"
#include "util.hpp"
using namespace std;

#define PROMPT          cout << ">>> ";
#define VERTICES        "vertices"
#define EDGES           "edges"
#define COMPONENTS      "components"
#define CONNECTED       "connected"
#define COMPONENT_SIZE  "componentSize"
#define DISTANCE        "distance"
#define CYCLE1          "inCycle1"
#define CYCLE2          "inCycle2"


static bool isValidAction(string action);
static void mostreUso (char *nomePrograma);


int main(int argc, char *argv[]) {
  Graph * graph;
  fstream file;
  string word, action;
  clock_t start, end; 
  double elapsed = 0;

  if (argc < 3)
    mostreUso(argv[0]);

  file.open(argv[1]);

  if (file.fail()) {
    cout << "ERRO: arquivo " << argv[1] << " nao pode ser aberto.\n";
    exit(EXIT_FAILURE);
  }

  graph = new Graph(strtol(argv[2], nullptr, 10));

  start = clock(); 

  while (file >> word)
    graph->insert(word);
  
  end = clock();
  elapsed = ((double) (end - start)) / CLOCKS_PER_SEC;
  cout << "arquivo lido e grafo construido em " << elapsed << " segundos\n";

  file.close();

  cout << "Possiveis operacoes do teste interativo:\n";
  cout << "vertices, edges, components, connected, componentSize <word>, distance <word1> <word2>, inCycle1 <word>, inCycle2 <word1, word2>\n";
  cout << "CRTL-D para encerrar.\n";
  PROMPT;

  while (getline(cin, word)) {
    stringstream line(word);
    string a, b;

    getline(line, action, ' ');
    getline(line, a, ' ');
    getline(line, b);

    if (action.empty()) {
      ERROR(operacao esperada);
    }
    else if (!isValidAction(action)) {
      ERROR(operacao nao reconhecida);
    }
    else if (action == VERTICES) {
      cout << "Quantidade de vértices: " << graph->vertices() << endl;
      cout << "O grau médio dos vértices é: " << graph->averageVerticesDegree() << endl;
    }
    else if (action == EDGES) {
      cout << "Quantidade de arestas: " << graph->edges() << endl;
    }
    else if (action == COMPONENTS) {
      cout << "Quantidade de componentes: " << graph->components() << endl;
    }
    else if (action == CONNECTED) {
      if (graph->isConnected())
        cout << "O grafo É conexo!" << endl;
      else
        cout << "O grafo NÃO É conexo!" << endl;
    }
    else {
      /* operação necessita de uma palavra */
      if (a.empty()) {
        ERROR(operacao necessita uma palavra);
      }
      else {
        if (action == COMPONENT_SIZE) {
          cout << "O tamanho da componente com a palavra " << a << " é " << graph->componentSize(a) << endl; 
        }
        else if (action == CYCLE1) {
          if (graph->inCycle(a))
            cout << "O grafo TEM um ciclo com a palavra " << a << endl;
          else
            cout << "O grafo NÃO TEM um ciclo com a palavra " << a << endl;
        }
        else {
          /* operação necessita de mais uma palavra */
          if (b.empty()) {
            ERROR(operacao necessita mais uma palavra);
          }
          else if (action == DISTANCE) {
            cout << "A distância entre as palavras " << a << " e " << b << " no grafo é " << graph->distance(a, b) << endl;
          }
          else if (action == CYCLE2) {
            if (graph->inCycle(a, b))
              cout << "O grafo TEM um ciclo com as palavras " << a << " e " << b << endl;
            else
              cout << "O grafo NÃO TEM um ciclo com as palavras " << a << " e " << b << endl;
          }
        }
      }
    }
    
    PROMPT;
  }

  cout << endl;

  delete graph;

  return 0;
}

static bool isValidAction(string action) {
  return action == VERTICES || action == EDGES || action == COMPONENTS || action == CONNECTED
      || action == COMPONENT_SIZE || action == DISTANCE || action == CYCLE1 || action == CYCLE2;
}

static void mostreUso (char *nomePrograma) {
  cout << "Uso \n"
    << "prompt> " << nomePrograma << " nome-arquivo número-caracteres\n"
    << "    nome-arquivo = nome do arquivo com o texto\n"
    << "    número-caracteres = número mínimo de caracteres que uma palavra do texto deve ter\n";
  
  exit(EXIT_FAILURE);
}
