#include "util.hpp"
#include <vector>

static vector<int> getWordsSubtraction(string a, string b);

bool checkNeighborsByDeletedLetter(string a, string b) {
  string biggerWord, smallerWord;
  int lengthA = a.length();
  int lengthB = b.length();
  int smallerIndex = 0;

  if (lengthA == lengthB + 1) {
    biggerWord = a;
    smallerWord = b;
  }
  else if (lengthB == lengthA + 1) {
    biggerWord = b;
    smallerWord = a;
  }
  else {
    return false;
  }

  for (int i = 0; i < (int) biggerWord.length(); i++)
    if (biggerWord[i] == smallerWord[smallerIndex])
      smallerIndex += 1;

  return (smallerIndex == (int) smallerWord.length()) ? true : false;
}

bool checkNeighborsBySwapLetter(string a, string b) {
  vector<int> wordsSubtraction;
  int countDifferentLetters = 0;
  int pos1 = -1, pos2 = -1;

  if (a.length() != b.length())
    return false;

  wordsSubtraction = getWordsSubtraction(a, b);

  for (int i = 0; i < (int) wordsSubtraction.size() && countDifferentLetters <= 2; i++) {
    if (wordsSubtraction[i] != 0) {
      countDifferentLetters += 1;

      if (countDifferentLetters <= 2) {
        if (pos1 == -1)
          pos1 = i;
        else
          pos2 = i;
      }
    }
  }

  if (countDifferentLetters == 2)
    if (a[pos1] == b[pos2] && a[pos2] == b[pos1])
      return true;
  
  return false;
}

bool checkNeighborsByReplaceLetter(string a, string b) {
  vector<int> wordsSubtraction;
  int countDifferentLetters = 0;

  if (a.length() != b.length())
    return false;

  wordsSubtraction = getWordsSubtraction(a, b);

  for (int i = 0; i < (int) wordsSubtraction.size() && countDifferentLetters <= 1; i++)
    if (wordsSubtraction[i] != 0)
      countDifferentLetters += 1;
  
  return (countDifferentLetters == 1) ? true : false;
}

static vector<int> getWordsSubtraction(string a, string b) {
  vector<int> wordsSubtraction;
  
  for (int i = 0; i < (int) a.length(); i++)
    wordsSubtraction.push_back(abs(a[i] - b[i]));

  return wordsSubtraction;
}
