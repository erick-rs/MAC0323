#ifndef _GRAPH_H
#define _GRAPH_H

#include <string>
#include <vector>
using namespace std;

class Graph {
  private:
    vector<vector<pair<int, string>>> graph;
    int K; // minimum number of letters of the graph words
    int V; // number of vertices
    int E; // number of edges
    void addEdge(int u, string wordU, int v, string wordV);
    void dfs(int v, vector<bool> & marked); // recursive depth first search
    void findCycles(int v, int p, vector<int> & colors, vector<int> & previous, vector<int> & mark, int cycleNumber);
    int searchWordInGraph(string word);
  
  public:
    Graph(int k);
    int insert(string word);
    int vertices();
    int averageVerticesDegree();
    int edges();
    int components();
    bool isConnected();
    int componentSize(string word);
    int distance(string a, string b);
    bool inCycle(string a);
    bool inCycle(string a, string b);
    void printGraph();
};

#endif
