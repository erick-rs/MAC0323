#ifndef _UTIL_H
#define _UTIL_H

#include <string>
using namespace std;

#define ERROR(msg) fprintf(stderr,"ERROR: %s\n", #msg)

bool checkNeighborsByDeletedLetter(string a, string b);
bool checkNeighborsBySwapLetter(string a, string b);
bool checkNeighborsByReplaceLetter(string a, string b);

#endif
