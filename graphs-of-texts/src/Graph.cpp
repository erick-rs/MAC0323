#include <iostream>
#include <queue>
#include "Graph.hpp"
#include "util.hpp"
using namespace std;

Graph::Graph(int k) {
  this->K = k;
  this->V = 0;
  this->E = 0;
}

void Graph::addEdge(int u, string wordU, int v, string wordV) {
  graph[u].push_back(make_pair(v, wordV));
  graph[v].push_back(make_pair(u, wordU));
}

void Graph::dfs(int indexVertice, vector<bool> & marked) {
  marked[indexVertice] = true;

  for (int i = 1; i < (int) graph[indexVertice].size(); i++) {
    int w = graph[indexVertice].at(i).first;

    if (!marked[w])
      dfs(w, marked);
  }
}

void Graph::findCycles(int u, int p, vector<int> & colors, vector<int> & previous, vector<int> & mark, int cycleNumber) {
  // v is the current vertex
  // p is the previous vertex
  if (colors[u] == 2)
    return;

  // detect cycle
  if (colors[u] == 1) {
    int currentVertex = p;
    
    cycleNumber += 1;
    mark[currentVertex] = cycleNumber;

    // back to the previous one until you reach the vertex v
    while (currentVertex != u) {
      currentVertex = previous[currentVertex];
      mark[currentVertex] = cycleNumber;
    }

    return;
  }

  previous[u] = p;
  colors[u] = 1;

  for (int v = 1; v < (int) graph[u].size(); v++) {
    int w = graph[u].at(v).first;

    if (w == previous[u])
      continue;

    findCycles(w, u, colors, previous, mark, cycleNumber);
  }

  colors[u] = 2;
}

int Graph::searchWordInGraph(string word) {
  // return the number of the vertex if the word is in graph or -1 else
  int index = -1;

  for (int v = 0; v < V; v++) {
    if (word == graph[v].at(0).second) {
      index = v;
      break;
    }
  }

  return index;
}

int Graph::insert(string word) {
  int countNewEdges = 0;

  if ((int) word.length() < K)
    return -1;
  
  if (searchWordInGraph(word) != -1)
    return -1;

  graph.push_back(vector<pair<int, string>>());
  graph[V].push_back(make_pair(V, word));

  for (int v = 0; v < V; v++) {
    string s = graph[v].at(0).second;

    if (checkNeighborsByReplaceLetter(word, s) || checkNeighborsByDeletedLetter(word, s) || checkNeighborsBySwapLetter(word, s)) {
      addEdge(v, s, V, word);
      countNewEdges += 1;
    }
  }

  V += 1;
  E += countNewEdges;

  return countNewEdges;
}

int Graph::vertices() {
  return V;
}

int Graph::edges() {
  return E;
}

int Graph::averageVerticesDegree() {
  int sum = 0;

  for (int v = 0; v < V; v++)
    sum += graph[v].size() - 1;

  return (sum / V);
}

int Graph::components() {
  vector<bool> marked(V, false);
  int numberOfComponents = 0;

  for (int v = 0; v < V; v++) {
    if (!marked[v]) {
      numberOfComponents += 1;
      dfs(v, marked);
    }
  }

  return numberOfComponents;
}

bool Graph::isConnected() {
  vector<bool> marked(V, false);
  dfs(0, marked);

  for (int v = 0; v < V; v++)
    if (!marked[v])
      return false;
  
  return true;
}

int Graph::componentSize(string word) {
  int indexVertice = searchWordInGraph(word);

  if (indexVertice != -1) {
    vector<bool> marked(V, false);
    int size = 0;

    dfs(indexVertice, marked);

    for (int v = 0; v < V; v++)
      if (marked[v])
        size += 1;

    return size;
  }

  return -1;
}

int Graph::distance(string a, string b) {
  int indexA = searchWordInGraph(a);
  int indexB = searchWordInGraph(b);

  if (indexA != -1 && indexB != -1) {
    vector<int> dist(V, -1);
    queue<int> queue;
    bool wasFound = false;

    queue.push(indexA);
    dist[indexA] = 0;

    while (!queue.empty() && !wasFound) {
      int u = queue.front();
      queue.pop();

      for (int i = 1; i < (int) graph[u].size() && !wasFound; i++) {
        int w = graph[u].at(i).first;

        if (dist[w] == -1) {
          queue.push(w);
          dist[w] = dist[u] + 1;
        }

        if (w == indexB)
          wasFound = true;
      }
    }

    return dist[indexB];
  }

  return -1;
}

bool Graph::inCycle(string a) {
  int indexVertice = searchWordInGraph(a);

  if (indexVertice != -1) {
    vector<int> colors(V, 0);
    vector<int> previous(V, -1);
    vector<int> mark(V, -1); // mark which cycle the vertex are.
    int cycleNumber = 0;

    findCycles(0, -1, colors, previous, mark, cycleNumber);

    if (mark[indexVertice] != -1)
      return true;
  }

  return false;
}

bool Graph::inCycle(string a, string b) {
  int indexA = searchWordInGraph(a);
  int indexB = searchWordInGraph(b);

  if (indexA != -1 && indexB != -1) {
    vector<int> colors(V, 0);
    vector<int> previous(V, -1);
    vector<int> mark(V, -1); // mark which cycle the vertex are.
    int cycleNumber = 0;

    findCycles(0, -1, colors, previous, mark, cycleNumber);

    if (mark[indexA] != -1 && (mark[indexA] == mark[indexB]))
      return true;
  }

  return false;
}

void Graph::printGraph() {
  for (int v = 0; v < V; v++) {
    cout << graph[v].at(0).second << endl;

    for (int w = 1; w < (int) graph[v].size(); w++)
      cout << "Vertice " << graph[v].at(w).first << ": " << graph[v].at(w).second << endl;

    cout << endl << endl;
  }
}
