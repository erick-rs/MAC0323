# Aeroporto

Sistema de gerenciamento de pistas de aeroporto feito em C++ para a disciplina MAC0323 - Algoritmos e Estruturas de Dados II.
O objetivo desse exercício-programa é treinar a implementação de algumas estruturas de dados básicas vistas em MAC0121 - Algoritmos e Estruturas de Dados I, prezando por uma implementação eficiente.
Para saber mais sobre como funciona o projeto, veja o arquivo enunciado.pdf em ./docs/

## Executando sistema
Para executar o sistema, é necessário executar dois comandos no diretório raiz do projeto:

1. make
2. ./program

Explicação: o comando make irá compilar os arquivos necessários e ./program executa o sistema.

