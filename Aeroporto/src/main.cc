#include <iostream>
#include "headers/Airport.h"
#include "headers/util.h"
using namespace std;

int main() {
    Airplane * airplane;
    Airport * airport;
    string airportName = "GRU";
    int simulationTime;
    int action;
    int takeOffs = 0, lands = 0, emergencyLands = 0, emergencyTakeoffs = 0;
    int seed;
    int seedOption, numOfPlanesOption;

    cout << "********* SISTEMA DE GERENCIAMENTO DE AEROPORTO DE GUARULHOS *********\n\n";
    cout << "Informe o tempo de simulação (em unidades de tempo): ";
    cin >> simulationTime;
    cout << "\nDeseja digitar uma semente? (1 pra sim e 0 pra não): ";
    cin >> seedOption;

    if (seedOption) {
        cout << "\nDigite a semente: ";
        cin >> seed;
        srand(seed);
    }
    else
        srand(time(NULL));
        
    cout << "\nDeseja simular a quantidade de aviões a cada instante manualmente? (1 pra sim e 0 pra não): ";
    cin >> numOfPlanesOption;
    
    airport = new Airport();

    for (int time = 0; time < simulationTime; time++) {
        int numberOfPlanes = rand() % K;

        if (numOfPlanesOption) {
            cout << endl << "Quantos aviões vao se comunicar com a torre (máximo 4): ";
            cin >> numberOfPlanes;
        }

        cout << "\nInstante " << time << ":\n";
        cout << "   (" << numberOfPlanes << " aviões enviam sinais)\n";
        while (numberOfPlanes > 0) {
            airplane = new Airplane(airportName, numOfPlanesOption);
            action = airplane->getAction();
            
            airport->addAirplaneOnQueue(airplane);
            cout << "   " << airplane->getName() << " ";
            cout << airplane->getOriginAndDestiny() << " - ";
            cout << actionStrings[airplane->getAction()] << " - ";

            if (airplane->getAction() == LAND || airplane->getAction() == EMERGENCY_LAND)
                cout << airplane->getFuel() << " unidades de combustível\n";
            else
                cout << "Tempo de voo: " << airplane->getFlightTime() << " unidades\n";

            numberOfPlanes -= 1;
        }

        cout << "\n   Saída:" << endl;
        airport->manageAirport();
    }

    delete airport;

    return EXIT_SUCCESS;
}
