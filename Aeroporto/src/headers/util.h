#ifndef UTIL_H
#define UTIL_H

#include <vector>

#define K 4 /* quantidade máxima de aviões que podem se comunicar com a torre */
#define C 20 /* tempo de combustível */
#define V 20 /* tempo de voo */

enum airplaneAction {
    TAKEOFF,
    LAND,
    EMERGENCY_TAKEOFF,
    EMERGENCY_LAND
};

const vector<string> actionStrings = {
    "Decolar", "Pousar", "Emergência de decolagem", "Emergência de pouso"
};

const vector<string> airlines = {
    "AD", "JJ", "G3", "AR", "06"
};

const vector<string> airports = {
    "BSB", "CGH", "GIG", "SSA", "FLN",
    "POA", "VCP", "REC", "CWB", "BEL",
    "VIX", "SDU", "CGB", "CGR", "FOR",
    "MCP", "MGF", "GYN", "NVT", "MAO",
    "NAT", "BSP", "MCZ", "PMW", "SLZ",
    "CCM", "LDB", "PVH", "RBR", "JOI"
};

const vector<int> flightTimes = {
    60, 105, 75, 100, 60, 75, 145, 100, 70, 90,
    65, 190, 75, 70, 80, 105, 80, 210, 60, 130,
    125, 120, 175, 100, 155, 95, 135, 75, 100, 90
};

#endif