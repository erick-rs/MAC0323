#ifndef QUEUE_H
#define QUEUE_H

#include "AbstractQueue.h"
#include "Airplane.h"

class Queue : public AbstractQueue<Airplane *> {
    private:
        int sumTakeOffWaitingTime;
        int sumLandWaitingTime;
        int countEmergenciesWithMinFuel(int fuel);
        bool willTheAirplanesFall(Node<Airplane *> * currentNode, int position);

    public:
        int decreaseAirplaneFuel();
        void increaseAirplaneWaitingTime();
        int getSumWaitingTime(int option);
        bool addWithPriority(Airplane * airplane, int option);
        void changeAirplanesToEmergency(Queue * queue, Queue * landEmergency, Queue * takeOffEmergency);
        void printQueue(int option);
};

#endif