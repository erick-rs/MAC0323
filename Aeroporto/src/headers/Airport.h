#ifndef AIRPORT_H
#define AIRPORT_H

#include "Queue.h"
#include "Runway.h"
#include "Airplane.h"

class Airport {
    private:
        int numberOfTakeOff;
        int numberOfLands;
        int numberOfTakeOffEmergencies;
        int numberOfLandEmergencies;
        int numberOfLanded;
        int availableFuel;
        int sumFuel;
        int sumLandedFuel;
        int sumTakeOffWaitingTime;
        int sumLandWaitingTime;
        bool isFirstIteration;

        Queue * queue;
        Queue * landEmergencyQueue;
        Queue * takeOffEmergencyQueue;
        Runway * runway1;
        Runway * runway2;
        Runway * runway3;
        Airplane * dequeueByPriorityQueue();
        Airplane * dequeueToRunway3();
        void planeToAnotherAirport();
        void setRunwayAvailability(Runway * runway);
        void printStatistics();
        double averageTime(int option);
        double averageFuel(bool didntLand);

    public:
        Airport();
        ~Airport();
        void manageAirport();
        void checkRunway(Runway * runway, bool isRunway3);
        void printAirplane(Airplane * airplane, string runwayName);
        void printAllQueues();
        void addAirplaneOnQueue(Airplane * airplane);
};

#endif