#ifndef NODE_H
#define NODE_H

template <class T>
class Node {
    private:
        Node<T> * previousNode;
        Node<T> * nextNode;
        T item;

    public:
        Node();
        ~Node();
        Node<T> * previous();
        Node<T> * next();
        T getItem();
        void setPrevious(Node<T> * previousNode);
        void setNext(Node<T> * nextNode);
        void setItem(T item);
};

template <class T>
Node<T>::Node() { }

template <class T>
Node<T>::~Node() { }

template <class T>
Node<T> * Node<T>::previous() {
    return this->previousNode;
}

template <class T>
T Node<T>::getItem() {
    return this->item;
}

template <class T>
Node<T> * Node<T>::next() {
    return this->nextNode;
}

template <class T>
void Node<T>::setPrevious(Node<T> * previousNode) {
    this->previousNode = previousNode;
}

template <class T>
void Node<T>::setNext(Node<T> * nextNode) {
    this->nextNode = nextNode;
}

template <class T>
void Node<T>::setItem(T item) {
    this->item = item;
}

#endif