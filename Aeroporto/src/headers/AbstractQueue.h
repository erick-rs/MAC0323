#ifndef ABSTRACTQUEUE_H
#define ABSTRACTQUEUE_H

#include "Node.h"
#include <iostream>

template <class T>
class AbstractQueue {
    private:
        Node<T> * firstNode;
        Node<T> * lastNode;
        int size;

    public:
        AbstractQueue();
        ~AbstractQueue();
        void add(T t);
        T dequeue();
        int getSize();
        bool isEmpty();
        Node<T> * getFirst();
        Node<T> * getLast();
        void setFirst(Node<T> * first);
        void setLast(Node<T> * last);
        void setSize(int size);
};

template <class T>
AbstractQueue<T>::AbstractQueue() {
    this->firstNode = nullptr;
    this->lastNode = this->firstNode;
    this->size = 0;
}

template <class T>
AbstractQueue<T>::~AbstractQueue() {
    while (!this->isEmpty()) {
        Node<T> * node = this->firstNode;
        this->firstNode = this->firstNode->next();

        if (std::is_pointer<T>::value)
            delete node->getItem();
        
        delete node;
    }
}

template <class T>
void AbstractQueue<T>::add(T t) {
    Node<T> * node = new Node<T>();
    node->setItem(t);

    if (this->isEmpty()) {
        node->setPrevious(nullptr);
        node->setNext(nullptr);
        this->firstNode = node;
        this->lastNode = this->firstNode;
    }
    else {
        node->setPrevious(this->lastNode);
        node->setNext(nullptr);
        this->lastNode->setNext(node);
        this->lastNode = node;
    }

    this->size += 1;
}

template <class T>
T AbstractQueue<T>::dequeue() {
    T item = this->firstNode->getItem();
    Node<T> * deleted = this->firstNode;
    this->firstNode = this->firstNode->next();

    if (this->size > 1)
        this->firstNode->setPrevious(nullptr);
    
    this->size -= 1;
    delete deleted;

    return item;
}

template <class T>
Node<T> * AbstractQueue<T>::getFirst() {
    return this->firstNode;
}

template <class T>
Node<T> * AbstractQueue<T>::getLast() {
    return this->lastNode;
}

template <class T>
void AbstractQueue<T>::setFirst(Node<T> * first) {
    this->firstNode = first;
}

template <class T>
void AbstractQueue<T>::setLast(Node<T> * last) {
    this->lastNode = last;
}

template <class T>
void AbstractQueue<T>::setSize(int size) {
    this->size = size;
}

template <class T>
int AbstractQueue<T>::getSize() {
    return this->size;
}

template <class T>
bool AbstractQueue<T>::isEmpty() {
    return this->firstNode == nullptr;
}

#endif