#ifndef AIRPLANE_H
#define AIRPLANE_H

#include <string>
using namespace std;

class Airplane {
    private:
        string name;
        string origin;
        string destiny;
        int fuel;
        int action; // TAKEOFF, LANDING OR EMERGENCY
        int flightTime;
        int waitingTime; // somente para avioes que vao decolar
        void createRandom(string airportName);
        void createManual(string airportName);

    public:
        Airplane(string airport, int option);
        ~Airplane();
        string getName();
        string getOriginAndDestiny();
        int getFuel();
        int getAction();
        int getFlightTime();
        int getWaitingTime(); // somente para avioes que vao decolar
        void setFuel(int fuel);
        void setAction(int action);
        void setFlightTime(int flightTime);
        void increaseWaitingTime(); // somente para avioes que vao decolar
};

#endif