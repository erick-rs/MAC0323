#ifndef RUNWAY_H
#define RUNWAY_H

#include <string>
using namespace std;

class Runway {
    private:
        string description;
        int action;
        int interdictedTime;
        bool free;

    public:
        Runway();
        Runway(string description, int action);
        ~Runway();
        string getDescription();
        int getAction();
        int getInterdictedTime();
        bool isFree();
        void setAction(int action);
        void setInterdictedTime(int interdictedTime);
        void setFree(bool free);
};

#endif