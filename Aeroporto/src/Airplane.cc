#include "headers/Airplane.h"
#include "headers/util.h"

#include <iostream>
using namespace std;

Airplane::Airplane(string airportName, int option) {
    this->name = "";
    this->name += airlines[rand() % 5];
    this->name += to_string(100 + (rand() % 900));
    this->waitingTime = 0;

    if (option)
        createManual(airportName);
    else
        createRandom(airportName);

    if (this->action == LAND || this->action == EMERGENCY_LAND) {
        this->origin = airports[rand() % 30];
        this->destiny = airportName;
    }
    else {
        this->origin = airportName;
        this->destiny = airports[rand() % 30];
    }
}

void Airplane::createRandom(string airportName) {
    int random = rand() % 100;

    if (random > 25)
        this->action = rand() % 2;
    else
        this->action = 2 + rand() % 2;

    this->name = "";
    this->name += airlines[rand() % 5];
    this->name += to_string(100 + (rand() % 900));
    this->fuel = 1 + rand() % C;
    this->flightTime = flightTimes[rand() % 30];
}

void Airplane::createManual(string airportName) {
    cout << "\nCombustível: ";
    cin >> this->fuel;
    cout << "\n0 para decolar, 1 para pousar, 2 emergencia de decolar e 3 para emergencia de pousar: ";
    cin >> this->action;
    cout << "\nTempo de voo: ";
    cin >> this->flightTime;
    cout << "\n-----------------------------------------------------------------\n";
} 

Airplane::~Airplane() {
    
}

string Airplane::getName() {
    return this->name;
}

string Airplane::getOriginAndDestiny() {
    return this->origin + "/" + this->destiny;
}

int Airplane::getFuel() {
    return this->fuel;
}

int Airplane::getAction() {
    return this->action;
}

int Airplane::getFlightTime() {
    return this->flightTime;
}

int Airplane::getWaitingTime() {
    return this->waitingTime;
}

void Airplane::setFuel(int fuel) {
    this->fuel = fuel;
}

void Airplane::setAction(int action) {
    this->action = action;
}

void Airplane::setFlightTime(int flightTime) {
    this->flightTime = flightTime;
}

void Airplane::increaseWaitingTime() {
    this->waitingTime += 1;
}