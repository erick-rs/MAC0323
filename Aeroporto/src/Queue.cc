#include "headers/Queue.h"
#include "headers/util.h"
#include <iostream>
using namespace std;

int Queue::countEmergenciesWithMinFuel(int fuel) {
    Node<Airplane *> * node = this->getFirst();
    int countMin = 0;

    while (node != nullptr) {
        if (node->getItem()->getFuel() <= fuel)
            countMin += 1;

        node = node->next();
    }

    return countMin;
}

bool Queue::willTheAirplanesFall(Node<Airplane *> * currentNode, int position) {
    while (currentNode != nullptr) {
        if (currentNode->getItem()->getFuel() < this->getSize() + 1 - position)
            return true;
        
        currentNode = currentNode->next();
        position -= 1;
    }

    return false;
}

int Queue::decreaseAirplaneFuel() {
    Node<Airplane *> * node = this->getFirst();
    Airplane * airplane;
    int sum = 0;

    while (node != nullptr) {
        airplane = node->getItem();
        
        if (airplane->getAction() == LAND) {
            airplane->setFuel(airplane->getFuel() - 1);
            sum += airplane->getFuel();
        }

        node = node->next();
    }

    return sum;
}

void Queue::increaseAirplaneWaitingTime() {
    Node<Airplane *> * node = this->getFirst();
    Airplane * airplane;
    this->sumTakeOffWaitingTime = 0;
    this->sumLandWaitingTime = 0;

    while (node != nullptr) {
        airplane = node->getItem();
        airplane->increaseWaitingTime();

        if (airplane->getAction() == TAKEOFF)
            this->sumTakeOffWaitingTime += airplane->getWaitingTime();
        else
            this->sumLandWaitingTime += airplane->getWaitingTime();

        node = node->next();
    }
}

int Queue::getSumWaitingTime(int option) {
    if (option == TAKEOFF)
        return this->sumTakeOffWaitingTime;
    
    return this->sumLandWaitingTime;
}

bool Queue::addWithPriority(Airplane * airplane, int option) {
    Node<Airplane *> * node = this->getFirst();
    Node<Airplane *> * aux;
    bool condition;
    int position = 0;

    if (node == nullptr)
        condition = true;
    else if (option == TAKEOFF)
        condition = airplane->getWaitingTime() >= node->getItem()->getWaitingTime();
    else
        condition = airplane->getFuel() >= node->getItem()->getFuel();

    while (node != nullptr && condition) {
        node = node->next();
        position += 1;

        if (node != nullptr) {
            if (option == TAKEOFF)
                condition = airplane->getWaitingTime() >= node->getItem()->getWaitingTime();
            else
                condition = airplane->getFuel() >= node->getItem()->getFuel();
        }
    }

    if (option == LAND && (this->willTheAirplanesFall(node, position + 1)
        || airplane->getFuel() <= this->countEmergenciesWithMinFuel(airplane->getFuel())))
        return false;
    
    if (node == nullptr) {
        this->add(airplane);
    }
    else { /* o avião (airplane) tem combustível menor que o avião que está na variável node  */
        Node<Airplane *> * newNode = new Node<Airplane *>();
        newNode->setItem(airplane);
        
        if (node == this->getFirst()) {
            node->setPrevious(newNode);
            newNode->setPrevious(nullptr);
            newNode->setNext(node);
            this->setFirst(newNode);
        }
        else {
            node->previous()->setNext(newNode);
            newNode->setPrevious(node->previous());
            node->setPrevious(newNode);
            newNode->setNext(node);
        }

        this->setSize(this->getSize() + 1);
    }

    return true;
}

void Queue::changeAirplanesToEmergency(Queue * queue, Queue * landEmergency, Queue * takeOffEmergency) {
    Node<Airplane *> * node;
    Node<Airplane *> * first;
    Node<Airplane *> * last;
    int i = 0;

    first = queue->getFirst();
    last = queue->getLast();

    node = last;
    while (node != nullptr) {
        bool condition;

        if (node->getItem()->getAction() == TAKEOFF) {
            condition = node->getItem()->getWaitingTime() > 0.1 * node->getItem()->getFlightTime(); 
        }
        else {
            condition = node->getItem()->getFuel() < queue->getSize() - i;
            i += 1;
        }

        if (condition) {
            Node<Airplane *> * next, * previous;
            Node<Airplane *> * aux;
            Airplane * airplane;
            bool successfullyAdded, willAirplaneLand;

            if (node->next() == nullptr) { // ultimo elemento
                next = node->next(); // null
                previous = node->previous(); // quando tem só um cara, previous = nullptr
                if (previous != nullptr)
                    previous->setNext(next);

                queue->setLast(previous);

                if (queue->getSize() == 1)
                    queue->setFirst(next);
            }
            else if (node == first) { // primeiro elemento
                next = node->next(); // second
                next->setPrevious(nullptr);
                queue->setFirst(next);
            }
            else {
                next = node->next();
                previous = node->previous();

                if (previous != nullptr)
                    previous->setNext(next);

                next->setPrevious(previous);
            }

            airplane = node->getItem();
            aux = node->previous();
            node->setNext(nullptr);
            node->setPrevious(nullptr);
            delete node;
            node = aux;
            queue->setSize(queue->getSize() - 1);

            if (airplane->getAction() == TAKEOFF)
                takeOffEmergency->addWithPriority(airplane, TAKEOFF);
            else {
                if (airplane->getFuel() <= 1 && landEmergency->getSize() > 0) {
                    cout << "   - Avião " << airplane->getName() << " " << airplane->getOriginAndDestiny()
                         << " é desviado para aeroporto vizinho\n";
                }
                else {
                    successfullyAdded = landEmergency->addWithPriority(airplane, LAND);
                    if (!successfullyAdded) {
                        cout << "   - Avião " << airplane->getName() << " " << airplane->getOriginAndDestiny()
                             << " é desviado para aeroporto vizinho\n";
                        delete airplane;
                    }
                }
            }
        }
        else
            node = node->previous();
    }
}

void Queue::printQueue(int option) {
    Node<Airplane *> * node = this->getFirst();

    while (node != nullptr) {
        cout << "   - Avião " << node->getItem()->getName() << " " << node->getItem()->getOriginAndDestiny();
        if (node->getItem()->getAction() == LAND)
            cout << " " << "está aguardando liberação para pouso\n";       
        else
            cout << " " << "está aguardando liberação para decolagem\n";

        node = node->next();
    }
}
