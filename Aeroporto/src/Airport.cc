#include "headers/Airport.h"
#include "headers/util.h"
#include <iostream>
#include <iomanip>
using namespace std;

Airport::Airport() {
    this->numberOfTakeOff = 0;
    this->numberOfLands = 0;
    this->numberOfLanded = 0;
    this->numberOfLandEmergencies = 0;
    this->numberOfTakeOffEmergencies = 0;
    this->sumFuel = 0;
    this->isFirstIteration = true;
    this->sumLandedFuel = 0;
    this->sumTakeOffWaitingTime = 0;
    this->sumLandWaitingTime = 0;

    this->queue = new Queue();
    this->landEmergencyQueue = new Queue();
    this->takeOffEmergencyQueue = new Queue();
    this->runway1 = new Runway("Pista 1", LAND);
    this->runway2 = new Runway("Pista 2", LAND);
    this->runway3 = new Runway("Pista 3", TAKEOFF);
}

Airport::~Airport() {
    delete queue;
    delete landEmergencyQueue;
    delete takeOffEmergencyQueue;
    delete runway1;
    delete runway2;
    delete runway3;
}

void Airport::manageAirport() {
    Airplane * airplane;
    string runwayName;

    this->numberOfLanded = 0;
    this->numberOfLandEmergencies = 0;
    this->numberOfTakeOffEmergencies = 0;
    this->sumLandedFuel = 0;

    if (!queue->isEmpty())
        queue->changeAirplanesToEmergency(queue, landEmergencyQueue, takeOffEmergencyQueue);


    this->sumTakeOffWaitingTime = queue->getSumWaitingTime(TAKEOFF);
    this->sumLandWaitingTime = queue->getSumWaitingTime(LAND);
    this->sumTakeOffWaitingTime += takeOffEmergencyQueue->getSumWaitingTime(TAKEOFF);
    this->sumLandWaitingTime += landEmergencyQueue->getSumWaitingTime(LAND);
    
    checkRunway(runway3, true);
    checkRunway(runway1, false);
    checkRunway(runway2, false);

    planeToAnotherAirport();
    printAllQueues();

    setRunwayAvailability(runway1);
    setRunwayAvailability(runway2);
    setRunwayAvailability(runway3);

    printStatistics();

    queue->increaseAirplaneWaitingTime();
    takeOffEmergencyQueue->increaseAirplaneWaitingTime();
    landEmergencyQueue->increaseAirplaneWaitingTime();

    this->sumFuel = queue->decreaseAirplaneFuel();
    this->sumFuel += landEmergencyQueue->decreaseAirplaneFuel();
    isFirstIteration = false;
}

void Airport::checkRunway(Runway * runway, bool isRunway3) {
    Airplane * airplane;
    string runwayName;

    if (runway->isFree()) {
        if (isRunway3)
            airplane = this->dequeueToRunway3();
        else
            airplane = this->dequeueByPriorityQueue();

        if (airplane != nullptr) {
            runwayName = runway->getDescription();
            runway->setAction(airplane->getAction());
            runway->setFree(false);
            printAirplane(airplane, runwayName);
            delete airplane;
        }
    }
}

void Airport::planeToAnotherAirport() {
    if (runway1->isFree() || runway2->isFree() || runway3->isFree())
        return;

    if (!landEmergencyQueue->isEmpty()) {
        if (landEmergencyQueue->getFirst()->getItem()->getFuel() <= 1) {
            Airplane * airplane = landEmergencyQueue->dequeue();
            
            cout << "   - Avião " << airplane->getName() << " " << airplane->getOriginAndDestiny()
                         << " é desviado para aeroporto vizinho\n";
            delete airplane;
        }
    }

    if (!queue->isEmpty()) {
        if (queue->getFirst()->getItem()->getAction() == LAND && queue->getFirst()->getItem()->getFuel() <= 1) {
            Airplane * airplane = queue->dequeue();
            cout << "   - Avião " << airplane->getName() << " " << airplane->getOriginAndDestiny()
                         << " é desviado para aeroporto vizinho\n";
            delete airplane;
        }
    }
}

void Airport::printAirplane(Airplane * airplane, string runwayName) {
    cout << "   - Avião " << airplane->getName() << " " << airplane->getOriginAndDestiny();
    if (airplane->getAction() == TAKEOFF || airplane->getAction() == EMERGENCY_TAKEOFF)
        cout << " decolou na " << runwayName << endl;
    else 
        cout << " pousou na " << runwayName << endl;
}

void Airport::printAllQueues() {
    landEmergencyQueue->printQueue(LAND);
    takeOffEmergencyQueue->printQueue(TAKEOFF);
    queue->printQueue(TAKEOFF);
}

void Airport::addAirplaneOnQueue(Airplane * airplane) {
    switch (airplane->getAction()) {
        case EMERGENCY_LAND:
            landEmergencyQueue->addWithPriority(airplane, LAND);
            this->numberOfLands += 1;
            break;
        case EMERGENCY_TAKEOFF:
            takeOffEmergencyQueue->addWithPriority(airplane, TAKEOFF);
            this->numberOfTakeOff += 1;
            break;
        case TAKEOFF:
            queue->add(airplane);
            this->numberOfTakeOff += 1;
            break;
        case LAND:
            queue->add(airplane);
            this->numberOfLands += 1;
            break;
        default:
            break;
    }
}

void Airport::setRunwayAvailability(Runway * runway) {
    if (!runway->isFree())
        runway->setInterdictedTime(runway->getInterdictedTime() - 1);

    if (runway->getInterdictedTime() == 0) {
        runway->setFree(true);
        runway->setInterdictedTime(3);
    }
}

Airplane * Airport::dequeueByPriorityQueue() {
    Airplane * airplane;

    if (!landEmergencyQueue->isEmpty()) {
        airplane = landEmergencyQueue->dequeue();
        this->numberOfLands -= 1;
        this->numberOfLanded += 1;
        this->numberOfLandEmergencies += 1;
        this->sumLandedFuel += airplane->getFuel();
        if (!isFirstIteration)
            this->sumFuel -= airplane->getFuel();
        this->sumLandWaitingTime -= airplane->getWaitingTime();
    }
    else if (!takeOffEmergencyQueue->isEmpty()) {
        airplane = takeOffEmergencyQueue->dequeue();
        this->numberOfTakeOff -= 1;
        this->numberOfTakeOffEmergencies += 1;
        this->sumTakeOffWaitingTime -= airplane->getWaitingTime();
    }
    else if (!queue->isEmpty()) {
        airplane = queue->dequeue();

        if (airplane->getAction() == TAKEOFF) {
            this->numberOfTakeOff -= 1;
            this->sumTakeOffWaitingTime -= airplane->getWaitingTime();
        }
        else {
            this->numberOfLands -= 1;
            this->numberOfLanded += 1;
            this->sumLandedFuel += airplane->getFuel();
            if (!isFirstIteration)
                this->sumFuel -= airplane->getFuel();
            this->sumLandWaitingTime -= airplane->getWaitingTime();
        }
    }
    else
        airplane = nullptr;

    return airplane;
}

Airplane * Airport::dequeueToRunway3() {
    Airplane * airplane;

    if (!landEmergencyQueue->isEmpty()) {
        airplane = landEmergencyQueue->dequeue();
        this->numberOfLands -= 1;
        this->numberOfLanded += 1;
        this->numberOfLandEmergencies += 1;
        this->sumLandedFuel += airplane->getFuel();
        if (!isFirstIteration)
            this->sumFuel -= airplane->getFuel();
        this->sumLandWaitingTime -= airplane->getWaitingTime();
    }
    else if (!takeOffEmergencyQueue->isEmpty()) {
        airplane = takeOffEmergencyQueue->dequeue();
        this->numberOfTakeOff -= 1;
        this->numberOfTakeOffEmergencies += 1;
        this->sumTakeOffWaitingTime -= airplane->getWaitingTime();
    }
    else if (!queue->isEmpty() && queue->getFirst()->getItem()->getAction() == TAKEOFF) {
        airplane = queue->dequeue();
        this->numberOfTakeOff -= 1;
        this->sumTakeOffWaitingTime -= airplane->getWaitingTime();
    }
    else
        airplane = nullptr;

    return airplane;
}

void Airport::printStatistics() {
    cout << endl;
    cout << "Tempo médio de espera para pouso: " << fixed << setprecision(1) << averageTime(LAND) << endl;
    cout << "Tempo médio de espera para decolagem: " << fixed << setprecision(1) << averageTime(TAKEOFF) << endl;
    cout << "Combustível médio dos aviões que ainda não pousaram: " << fixed << setprecision(1) << averageFuel(true) << endl;
    cout << "Combustível médio dos aviões que pousaram: " << fixed << setprecision(1) << averageFuel(false) << endl;
    cout << "Quantidade de aviões que pousaram com emergência: " << this->numberOfLandEmergencies << endl;
    cout << "Quantidade de aviões que decolaram com emergência: " << this->numberOfTakeOffEmergencies << endl << endl;
}

double Airport::averageTime(int option) {
    double average = 0.0;

    if (option == TAKEOFF && numberOfTakeOff != 0)
        average = ((double) sumTakeOffWaitingTime) / numberOfTakeOff;
    else if (option == LAND && numberOfLands != 0)
        average = ((double) sumLandWaitingTime) / numberOfLands;

    return average;
}

double Airport::averageFuel(bool didntLand) {
    double average = 0.0;

    if (didntLand && numberOfLands != 0)
        average = ((double) sumFuel) / numberOfLands;
    else if (!didntLand && numberOfLanded != 0)
        average = ((double) sumLandedFuel) / numberOfLanded;

    return average;
}