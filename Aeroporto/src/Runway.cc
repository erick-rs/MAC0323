#include "headers/Runway.h"

Runway::Runway() { }

Runway::Runway(string description, int action) {
    this->description = description;
    this->action = action;
    this->interdictedTime = 3;
    this->free = true;
}

Runway::~Runway() { }

string Runway::getDescription() {
    return this->description;
}

int Runway::getAction() {
    return this->action;
}

int Runway::getInterdictedTime() {
    return this->interdictedTime;
}

bool Runway::isFree() {
    return this->free;
}

void Runway::setAction(int action) {
    this->action = action;
}

void Runway::setInterdictedTime(int interdictedTime) {
    this->interdictedTime = interdictedTime;
}

void Runway::setFree(bool free) {
    this->free = free;
}
