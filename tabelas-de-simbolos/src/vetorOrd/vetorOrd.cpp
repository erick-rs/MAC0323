#include "vetorOrd.hpp"

vetorOrd::vetorOrd(string nome_arquivo) {
  st = new Par*[10];
  n = 0;
  size = 10;
  this->readFile(nome_arquivo);
  printSt();
}

vetorOrd::~vetorOrd() {
  Chave chave = seleciona(0);

  while (chave != NOT_FOUND_KEY) {
    remove(chave);
    chave = seleciona(0);
  }
  for (int i = 0; i < n; i++)
    delete st[i];
  
  delete [] st;
}

int vetorOrd::buscaBinaria(Chave chave) {
  int ini = 0;
  int fim = n-1;
  int meio = 0;

  while (ini <= fim) {
    meio = (ini + fim)/2;

    if (st[meio]->chave == chave)
      return meio;
    else if (st[meio]->chave > chave)
      fim = meio - 1;
    else
      ini = meio + 1;
  }
  
  return -1;
}

void vetorOrd::insere(Chave chave, Valor valor) {
  if (n == size)
    resize();
    
  int ini = 0;
  int fim = n-1;
  int meio = 0;

  while (ini <= fim) {
    meio = (ini + fim)/2;

    if (st[meio]->chave == chave) {
      st[meio]->valor = st[meio]->valor + 1;
      break;
    }
    else if (st[meio]->chave > chave) {
      fim = meio - 1;
    }
    else {
      ini = meio + 1;
    }
  }

  if (ini > fim) {
    for (int j = n; j > ini; j--)
      st[j] = st[j-1];

    st[ini] = new Par(chave, valor);
    n += 1;
  }
}

Valor vetorOrd::devolve(Chave chave) {
  int indice = buscaBinaria(chave);
  return indice != -1 ? st[indice]->valor : NOT_FOUND_VALUE;
}

void vetorOrd::remove(Chave chave) {
  int pos = buscaBinaria(chave);

  if (pos != -1) {
    delete st[pos];
      
    for (int j = pos+1; j < n; j++)
      st[j-1] = st[j];
    
    st[n-1] = nullptr;
    
    n -= 1;
  }
}

int vetorOrd::rank(Chave chave) {
  int rank = buscaBinaria(chave);
  return rank != -1 ? rank : NOT_FOUND_VALUE;
}

Chave vetorOrd::seleciona(int k) {
  if (k >= 0 && k < n)
    return st[k]->chave;
  
  return NOT_FOUND_KEY;
}

void vetorOrd::resize() {
  Par ** newst = new Par*[2 * n];

  for (int i = 0; i < n; i++) {
    newst[i] = st[i];
    st[i] = nullptr;
  }

  delete [] st;
  st = newst;
  size = 2 * n;
}

void vetorOrd::printSt() {
  for (int i = 0; i < n; i++)
    cout << st[i]->chave << endl;
}
