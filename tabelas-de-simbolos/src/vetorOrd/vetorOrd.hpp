#ifndef _VETORORD_H
#define _VETORORD_H

#include "../st.hpp"
#include "../pair.hpp"

typedef Pair<Chave, Valor> Par;

class vetorOrd : public ST<Chave, Valor> {
  private:
    Par ** st;
    int n;
    int size;
    void resize();
    int buscaBinaria(Chave chave);

  protected:
    void printSt() override;
  
  public:
    vetorOrd(string nome_arquivo);
    virtual ~vetorOrd();
    void insere(Chave chave, Valor valor) override;
    Valor devolve(Chave chave) override;
    void remove(Chave chave) override;
    int rank(Chave chave) override;
    Chave seleciona(int k) override;
};

#endif
