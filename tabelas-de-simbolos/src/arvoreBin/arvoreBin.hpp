#ifndef _ARVOREBIN_H
#define _ARVOREBIN_H

#include "../st.hpp"
#include "../treeNode.hpp"

class arvoreBin : public ST<Chave, Valor> {
  private:
    TreeNode<Chave, Valor> * raiz;
    TreeNode<Chave, Valor> * newArvore(Chave chave, Valor valor);
    TreeNode<Chave, Valor> * put(TreeNode<Chave, Valor> * raiz, Chave chave, Valor valor);
    TreeNode<Chave, Valor> * destroy(TreeNode<Chave, Valor> * raiz, Chave chave);
    TreeNode<Chave, Valor> * max(TreeNode<Chave, Valor> * raiz);
    Valor get(TreeNode<Chave, Valor> * raiz, Chave chave);
    Chave select(TreeNode<Chave, Valor> * raiz, int k);
    int getRank(TreeNode<Chave, Valor> * raiz, Chave chave);
    int size(TreeNode<Chave, Valor> * raiz);
    void deleteSt(TreeNode<Chave, Valor> * raiz);

  protected:
    void printSt(TreeNode<Chave, Valor> * raiz);
  
  public:
    arvoreBin(string nome_arquivo);
    virtual ~arvoreBin();
    void insere(Chave chave, Valor valor) override;
    Valor devolve(Chave chave) override;
    void remove(Chave chave) override;
    int rank(Chave chave) override;
    Chave seleciona(int k) override;
};

#endif
