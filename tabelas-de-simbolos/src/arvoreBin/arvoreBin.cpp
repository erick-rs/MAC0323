#include "arvoreBin.hpp"

typedef TreeNode<Chave, Valor> No;

arvoreBin::arvoreBin(string nome_arquivo) {
  raiz = nullptr;
  this->readFile(nome_arquivo);
  printSt(raiz);
}

arvoreBin::~arvoreBin() {
  deleteSt(raiz);
}

void arvoreBin::deleteSt(No * raiz) {
  if (raiz == nullptr)
    return;

  deleteSt(raiz->esq);
  deleteSt(raiz->dir);
  delete raiz;
}

No * arvoreBin::newArvore(Chave chave, Valor valor) {
  No * newRaiz = new No();
  newRaiz->chave = chave;
  newRaiz->valor = valor;
  newRaiz->esq = nullptr;
  newRaiz->dir = nullptr;
  newRaiz->size = 1;
  return newRaiz;
}

No * arvoreBin::put(No * raiz, Chave chave, Valor valor) {
  if (raiz == nullptr)
    return newArvore(chave, valor);
  
  if (chave == raiz->chave)
    raiz->valor = raiz->valor + 1;
  else if (chave < raiz->chave)
    raiz->esq = put(raiz->esq, chave, valor);
  else
    raiz->dir = put(raiz->dir, chave, valor);

  raiz->size = size(raiz->esq) + size(raiz->dir) + 1;

  return raiz;
}

void arvoreBin::insere(Chave chave, Valor valor) {
  raiz = put(raiz, chave, valor);
}

Valor arvoreBin::get(No * raiz, Chave chave) {
  if (raiz == nullptr)
    return NOT_FOUND_VALUE;

  if (chave == raiz->chave)
    return raiz->valor;
  else if (chave < raiz->chave)
    return get(raiz->esq, chave);
  else
    return get(raiz->dir, chave);
}

Valor arvoreBin::devolve(Chave chave) {
  return get(raiz, chave);
}

No * arvoreBin::destroy(No * raiz, Chave chave) {
  if (raiz == nullptr)
    return raiz;

  if (chave < raiz->chave)
    raiz->esq = destroy(raiz->esq, chave);
  else if (chave > raiz->chave)
    raiz->dir = destroy(raiz->dir, chave);
  else {
    if (raiz->dir == nullptr) {
      No * aux = raiz->esq;
      delete raiz;
      return aux;
    }
    else if (raiz->esq == nullptr) {
      No * aux = raiz->dir;
      delete raiz;
      return aux;
    }
    else {
      No * aux = max(raiz->esq);
      raiz->chave = aux->chave;
      raiz->valor = aux->valor;
      raiz->esq = destroy(raiz->esq, aux->chave);
    }
  }

  raiz->size = size(raiz->esq) + size(raiz->dir) + 1;

  return raiz;
}

No * arvoreBin::max(No * raiz) {
  if (raiz == nullptr || raiz->dir == nullptr)
    return raiz;

  return max(raiz->dir);
}

void arvoreBin::remove(Chave chave) {
  raiz = destroy(raiz, chave);
}

int arvoreBin::size(No * raiz) {
  if (raiz == nullptr)
    return 0;
  
  return raiz->size;
}

int arvoreBin::getRank(No * raiz, Chave chave) {
  if (raiz == nullptr)
    return NOT_FOUND_VALUE;
    
  if (chave < raiz->chave)
    return getRank(raiz->esq, chave);
  else if (chave > raiz->chave)
    return size(raiz->esq) + 1 + getRank(raiz->dir, chave);
  else
    return size(raiz->esq);
}

int arvoreBin::rank(Chave chave) {
  if (devolve(chave) == NOT_FOUND_VALUE)
    return NOT_FOUND_VALUE;
  
  return getRank(raiz, chave);
}

Chave arvoreBin::select(No * raiz, int k) {
  if (raiz == nullptr)
    return NOT_FOUND_KEY;

  int r = rank(raiz->chave);
  
  if (r == k)
    return raiz->chave;
  else if (r > k)
    return select(raiz->esq, k);
  else
    return select(raiz->dir, k);
}

Chave arvoreBin::seleciona(int k) {
  return select(raiz, k);
}

void arvoreBin::printSt(No * raiz) {
  if (raiz == nullptr)
    return;

  printSt(raiz->esq);
  cout << raiz->chave << " : " << raiz->valor << endl;
  printSt(raiz->dir);
}
