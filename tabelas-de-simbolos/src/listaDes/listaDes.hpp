#ifndef _LISTADES_H
#define _LISTADES_H

#include "../st.hpp"
#include "../node.hpp"
 
class listaDes : public ST<Chave, Valor> {
  private:
    Node<Chave, Valor> * first;
  
  protected:
    void printSt() override;
  
  public:
    listaDes(string nome_arquivo);
    virtual ~listaDes();
    void insere(Chave chave, Valor valor) override;
    Valor devolve(Chave chave) override;
    void remove(Chave chave) override;
    int rank(Chave chave) override;
    Chave seleciona(int k) override;
};

#endif
