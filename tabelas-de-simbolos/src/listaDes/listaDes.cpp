#include "listaDes.hpp"

typedef Node<Chave, Valor> No;

listaDes::listaDes(string nome_arquivo) {
  first = nullptr;
  this->readFile(nome_arquivo);
  printSt();
}

listaDes::~listaDes() {
  while (first != nullptr) {
    No * node = first;
    first = first->next;
    delete node;
  }
}

void listaDes::insere(Chave chave, Valor valor) {
  No * node = first;
  No * anterior;

  while (node != nullptr) {
    if (chave == node->chave) {
      node->valor = node->valor + 1;
      return;
    }
    
    anterior = node;
    node = node->next;
  }

  node = new No();
  node->chave = chave;
  node->valor = valor;
  node->next = nullptr;

  if (first != nullptr)  
    anterior->next = node;
  else
    first = node;
}

Valor listaDes::devolve(Chave chave) {
  No * node = first;

  while (node != nullptr) {
    if (chave == node->chave)
      return node->valor;

    node = node->next;
  }

  return NOT_FOUND_VALUE;
}

void listaDes::remove(Chave chave) {
  No * node = first;
  No * anterior = nullptr;

  while (node != nullptr) {
    if (chave == node->chave) {
      if (anterior == nullptr) {
        anterior = node;
        node = node->next;
        first = node;
        delete anterior;
      }
      else {
        anterior->next = node->next;
        node->next = nullptr;
        delete node;
      }
      
      return;
    }

    anterior = node;
    node = node->next;
  }
}

int listaDes::rank(Chave chave) {
  No * node = first;
  int rank = 0;

  if (devolve(chave) == NOT_FOUND_VALUE)
    return NOT_FOUND_VALUE;

  while (node != nullptr) {
    Chave nodeChave = node->chave;

    if (chave > nodeChave)
      rank += 1;
    
    node = node->next;
  }

  return rank;
}

Chave listaDes::seleciona(int k) {
  No * node = first;

  while (node != nullptr) {
    if (rank(node->chave) == k)
      return node->chave;

    node = node->next;
  }

  return NOT_FOUND_KEY;
}

void listaDes::printSt() {
  No * node = first;

  while (node != nullptr) {
    cout << node->chave << endl;
    node = node->next;
  }
}
