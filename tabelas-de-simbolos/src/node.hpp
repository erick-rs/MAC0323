#ifndef _NODE_H
#define _NODE_H

template <class Key, class Value>
class Node {
  public:
    Key chave;
    Value valor;
    Node<Key, Value> * next;
    Node();
    ~Node();
};

template <class Key, class Value>
Node<Key, Value>::Node() { }

template <class Key, class Value>
Node<Key, Value>::~Node() { }

#endif
