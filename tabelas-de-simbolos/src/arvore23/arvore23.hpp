#ifndef _ARVORE23_H
#define _ARVORE23_H

#include "../st.hpp"
#include "../node23.hpp"

class arvore23 : public ST<Chave, Valor> {
  private:
    Node23<Chave, Valor> * raiz;
    Node23<Chave, Valor> * put(Node23<Chave, Valor> * raiz, Chave chave, Valor valor, bool & cresceu);
    Node23<Chave, Valor> * destroy(Node23<Chave, Valor> * raiz, Chave chave, bool & diminuiu);
    Node23<Chave, Valor> * max(Node23<Chave, Valor> * raiz);
    Valor get(Node23<Chave, Valor> * raiz, Chave chave);
    int size(Node23<Chave, Valor> * raiz);
    int getRank(Node23<Chave, Valor> * raiz, Chave chave);
    Chave select(Node23<Chave, Valor> * raiz, int k);
    void deleteSt(Node23<Chave, Valor> * raiz);
    void printSt(Node23<Chave, Valor> * raiz);
  
  public:
    arvore23(string nome_arquivo);
    virtual ~arvore23();
    void insere(Chave chave, Valor valor) override;
    Valor devolve(Chave chave) override;
    void remove(Chave chave) override;
    int rank(Chave chave) override;
    Chave seleciona(int k) override;
};

#endif
