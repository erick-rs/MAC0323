#include "arvore23.hpp"

typedef Node23<Chave, Valor> No;

arvore23::arvore23(string nome_arquivo) {
  raiz = nullptr;
  this->readFile(nome_arquivo);
  printSt(raiz);
}

arvore23::~arvore23() {
  deleteSt(raiz);
}

void arvore23::deleteSt(No * raiz) {
  if (raiz == nullptr)
    return;

  deleteSt(raiz->apt1);
  deleteSt(raiz->apt2);
  deleteSt(raiz->apt3);

  delete raiz;
}

No * arvore23::put(No * raiz, Chave chave, Valor valor, bool & cresceu) {
  if (raiz == nullptr) {
    raiz = new No();
    raiz->chave1 = chave;
    raiz->valor1 = valor; 
    raiz->pai = nullptr;
    raiz->ehDoisNo = true;
    cresceu = true;
  }
  else if (chave == raiz->chave1) {
    raiz->valor1 = raiz->valor1 + 1;
    cresceu = false;
  }
  else if (!raiz->ehDoisNo && chave == raiz->chave2) {
    raiz->valor2 = raiz->valor2 + 1;
    cresceu = false;
  }
  else if (raiz->ehFolha()) {
    if (raiz->ehDoisNo) {
      if (chave > raiz->chave1) {
        raiz->chave2 = chave;
        raiz->valor2 = valor;
      }
      else {
        raiz->chave2 = raiz->chave1;
        raiz->valor2 = raiz->valor1;
        raiz->chave1 = chave;
        raiz->valor1 = valor;
      }
      
      raiz->ehDoisNo = false;
      cresceu = false;
    }
    else {
      No * meio = new No();
      No * maior = new No();
      Chave maiorChave, chaveMeio;
      Valor maiorValor, valorMeio;

      if (chave > raiz->chave2) {
        maiorChave = chave;
        maiorValor = valor;
        chaveMeio = raiz->chave2;
        valorMeio = raiz->valor2;
      }
      else {
        maiorChave = raiz->chave2;
        maiorValor = raiz->valor2;

        if (chave > raiz->chave1) {
          chaveMeio = chave;
          valorMeio = valor;
        }
        else {
          chaveMeio = raiz->chave1;
          valorMeio = raiz->valor1;
          raiz->chave1 = chave;
          raiz->valor1 = valor;
        }
      }

      maior->chave1 = maiorChave;
      maior->valor1 = maiorValor;
      maior->pai = meio;
      meio->chave1 = chaveMeio;
      meio->valor1 = valorMeio;
      meio->pai = raiz->pai;
      meio->apt1 = raiz;
      meio->apt2 = maior;
      maior->ehDoisNo = true;
      meio->ehDoisNo = true;
      raiz->pai = meio;
      raiz->ehDoisNo = true;

      cresceu = true;
      raiz = meio;
    }
  }
  else if (chave < raiz->chave1) {
    No * p = put(raiz->apt1, chave, valor, cresceu);

    if (cresceu) {
      if (raiz->ehDoisNo) {
        raiz->chave2 = raiz->chave1;
        raiz->valor2 = raiz->valor1;
        raiz->apt3 = raiz->apt2;
        raiz->apt2->pai = raiz;
        raiz->chave1 = p->chave1;
        raiz->valor1 = p->valor1;
        raiz->apt1 = p->apt1;
        raiz->apt2 = p->apt2;
        p->apt1->pai = raiz;
        p->apt2->pai = raiz;
        raiz->ehDoisNo = false;
        cresceu = false;

        delete p;
      }
      else {
        No * maior = new No();

        maior->chave1 = raiz->chave2;
        maior->valor1 = raiz->valor2;
        maior->apt1 = raiz->apt2;
        maior->apt2 = raiz->apt3;
        raiz->apt2->pai = maior;
        raiz->apt3->pai = maior;
        maior->pai = raiz;
        maior->ehDoisNo = true;

        raiz->apt1 = p;
        raiz->apt2 = maior;
        raiz->apt3 = nullptr;
        raiz->ehDoisNo = true;

        p->pai = raiz;

        cresceu = true;
      }
    }
  }
  else if (!raiz->ehDoisNo && raiz->chave2 < chave) {
    No * p = put(raiz->apt3, chave, valor, cresceu);
    
    if (cresceu) {
      No * menor = new No();

      menor->chave1 = raiz->chave1;
      menor->valor1 = raiz->valor1;
      menor->apt1 = raiz->apt1;
      menor->apt2 = raiz->apt2;
      raiz->apt1->pai = menor;
      raiz->apt2->pai = menor;
      menor->pai = raiz;
      menor->ehDoisNo = true;

      raiz->chave1 = raiz->chave2;
      raiz->valor1 = raiz->valor2;
      raiz->apt1 = menor;
      raiz->apt2 = p;
      p->pai = raiz;
      raiz->apt3 = nullptr;
      raiz->ehDoisNo = true;

      cresceu = true;
    }
  }
  else {
    No * p = put(raiz->apt2, chave, valor, cresceu);

    if (cresceu) {
      if (raiz->ehDoisNo) {
        raiz->chave2 = p->chave1;
        raiz->valor2 = p->valor1;
        raiz->apt2 = p->apt1;
        raiz->apt3 = p->apt2;
        p->apt1->pai = raiz;
        p->apt2->pai = raiz;
        raiz->ehDoisNo = false;
        cresceu = false;

        delete p;
      }
      else {
        No * menor = new No();
        No * maior = new No();

        menor->chave1 = raiz->chave1;
        menor->valor1 = raiz->valor1;
        menor->apt1 = raiz->apt1;
        raiz->apt1->pai = menor;
        menor->apt2 = p->apt1;
        p->apt1->pai = menor;
        menor->pai = raiz;
        menor->ehDoisNo = true;

        maior->chave1 = raiz->chave2;
        maior->valor1 = raiz->valor2;
        maior->apt1 = p->apt2;
        p->apt2->pai = maior;
        maior->apt2 = raiz->apt3;
        raiz->apt3->pai = maior;
        maior->pai = raiz;
        maior->ehDoisNo = true;

        raiz->chave1 = p->chave1;
        raiz->valor1 = p->valor1;
        raiz->apt1 = menor;
        raiz->apt2 = maior;
        raiz->apt3 = nullptr;
        raiz->ehDoisNo = true;

        cresceu = true;

        delete p;
      }
    }
  }

  return raiz;
}

void arvore23::insere(Chave chave, Valor valor) {
  bool cresceu = true;
  raiz = put(raiz, chave, valor, cresceu);
}

Valor arvore23::get(No * raiz, Chave chave) {
  if (raiz == nullptr)
    return NOT_FOUND_VALUE;

  if (chave == raiz->chave1)
    return raiz->valor1;
  
  if (!raiz->ehDoisNo && chave == raiz->chave2)
    return raiz->valor2;

  if (chave < raiz->chave1)
    return get(raiz->apt1, chave);
  else if (!raiz->ehDoisNo && chave > raiz->chave2)
    return get(raiz->apt3, chave);
  else
    return get(raiz->apt2, chave);
}

Valor arvore23::devolve(Chave chave) {
  return get(raiz, chave);
}

No * arvore23::destroy(No * raiz, Chave chave, bool & diminuiu) {
  if (raiz == nullptr) {
    return raiz;
  }
  else if (raiz->pai == nullptr && raiz->ehFolha() && raiz->ehDoisNo && chave == raiz->chave1) {
    delete raiz;
    raiz = nullptr;
  }
  else if (chave == raiz->chave1) {
    if (!raiz->ehFolha()) {
      // pega o nó da folha com maior valor possível, substitui nessa posição e remove a folha
      No * aux = max(raiz->apt1);

      if (aux->ehDoisNo) {
        raiz->chave1 = aux->chave1;
        raiz->valor1 = aux->valor1;
        raiz->apt1 = destroy(raiz->apt1, aux->chave1, diminuiu);
      }
      else {
        raiz->chave1 = aux->chave2;
        raiz->valor1 = aux->valor2;
        raiz->apt1 = destroy(raiz->apt1, aux->chave2, diminuiu);
      }

      diminuiu = false;
    }
    else {
      if (!raiz->ehDoisNo) {
        raiz->chave1 = raiz->chave2;
        raiz->valor1 = raiz->valor2;
        raiz->ehDoisNo = true;
        diminuiu = false;
      }
      else {
        diminuiu = true;
      }
    }
  }
  else if (!raiz->ehDoisNo && chave == raiz->chave2) {
    if (!raiz->ehFolha()) {
      // pega o nó da folha com maior valor possível, substitui nessa posição e remove a folha
      No * aux = max(raiz->apt2);

      if (aux->ehDoisNo) {
        raiz->chave2 = aux->chave1;
        raiz->valor2 = aux->valor1;
        raiz->apt2 = destroy(raiz->apt2, aux->chave1, diminuiu);
      }
      else {
        raiz->chave2 = aux->chave2;
        raiz->valor2 = aux->valor2;
        raiz->apt2 = destroy(raiz->apt2, aux->chave2, diminuiu);
      }
      diminuiu = false;
    }
    else {
      raiz->ehDoisNo = true;
      diminuiu = false;
    }
  }
  else if (chave < raiz->chave1) {
    No * p = destroy(raiz->apt1, chave, diminuiu);

    if (diminuiu) {
      No * irmao = raiz->apt2;

      if (!raiz->ehDoisNo) {
        if (!irmao->ehDoisNo) {
          // desce a menor chave do pai pra raiz e sobe a menor chave do irmao pro pai
          p->chave1 = raiz->chave1;
          p->valor1 = raiz->valor1;
          raiz->chave1 = irmao->chave1;
          raiz->valor1 = irmao->valor1;
          irmao->chave1 = irmao->chave2;
          irmao->valor1 = irmao->valor2;
          irmao->ehDoisNo = true;
          
          if (p->apt1 == nullptr)
            p->apt1 = p->apt2;
          
          p->apt2 = irmao->apt1;

          if (irmao->apt1 != nullptr)
            irmao->apt1->pai = p;
          
          irmao->apt1 = irmao->apt2;
          irmao->apt2 = irmao->apt3;
          irmao->apt3 = nullptr;

          diminuiu = false;
        }
        else {
          irmao->chave2 = irmao->chave1;
          irmao->valor2 = irmao->valor1;
          irmao->chave1 = raiz->chave1;
          irmao->valor1 = raiz->valor1;
          irmao->ehDoisNo = false;
          raiz->chave1 = raiz->chave2;
          raiz->valor1 = raiz->valor2;
          raiz->apt1 = irmao;
          raiz->apt2 = raiz->apt3;
          raiz->apt3 = nullptr;
          raiz->ehDoisNo = true;

          irmao->apt3 = irmao->apt2;
          irmao->apt2 = irmao->apt1;
            
          if (p->apt1 == nullptr) {
            irmao->apt1 = p->apt2;

            if (p->apt2 != nullptr)
              p->apt2->pai = irmao;
          }
          else {
            irmao->apt1 = p->apt1;

            if (p->apt1 != nullptr)
              p->apt1->pai = irmao;
          }

          diminuiu = false;
          delete p;
        }
      }
      else {
        if (!irmao->ehDoisNo) {
          p->chave1 = raiz->chave1;
          p->valor1 = raiz->valor1;
          raiz->chave1 = irmao->chave1;
          raiz->valor1 = irmao->valor1;
          irmao->chave1 = irmao->chave2;
          irmao->valor1 = irmao->valor2;
          irmao->ehDoisNo = true;

          if (p->apt1 == nullptr)
            p->apt1 = p->apt2;
          
          p->apt2 = irmao->apt1;

          if (irmao->apt1 != nullptr)
            irmao->apt1->pai = p;
          
          irmao->apt1 = irmao->apt2;
          irmao->apt2 = irmao->apt3;
          irmao->apt3 = nullptr;

          diminuiu = false;
        }
        else {
          irmao->chave2 = irmao->chave1;
          irmao->valor2 = irmao->valor1;
          irmao->chave1 = raiz->chave1;
          irmao->valor1 = raiz->valor1;
          irmao->ehDoisNo = false;

          irmao->apt3 = irmao->apt2;
          irmao->apt2 = irmao->apt1;

          // TEM QUE PEGAR OS PONTEIRO DA RAIZ E COLOCAR NO IRMAO
          if (p->apt1 == nullptr) {
            irmao->apt1 = p->apt2;

            if (p->apt2 != nullptr)
              p->apt2->pai = irmao;
          }
          else {
            irmao->apt1 = p->apt1;

            if (p->apt1 != nullptr)
              p->apt1->pai = irmao;
          }

          raiz->apt1 = nullptr; // desvincula a raiz do pai

          diminuiu = true;

          delete p;

          if (raiz->pai == nullptr) {
            delete raiz;
            raiz = irmao;
            raiz->pai = nullptr;
          }
        }
      }
    }
  }
  else if (!raiz->ehDoisNo && chave > raiz->chave2) {
    No * p = destroy(raiz->apt3, chave, diminuiu);

    if (diminuiu) {
      No * irmao = raiz->apt2;

      if (!irmao->ehDoisNo) {
        p->chave1 = raiz->chave2;
        p->valor1 = raiz->valor2;
        raiz->chave2 = irmao->chave2;
        raiz->valor2 = irmao->valor2;
        irmao->ehDoisNo = true;

        if (p->apt1 == nullptr) {
          p->apt1 = irmao->apt3;
        }
        else {
          p->apt2 = p->apt1;
          p->apt1 = irmao->apt3;
        }

        if (irmao->apt3 != nullptr)
          irmao->apt3->pai = p;

        irmao->apt3 = nullptr;
        
        diminuiu = false;
      }
      else {
        irmao->chave2 = raiz->chave2;
        irmao->valor2 = raiz->valor2;
        irmao->ehDoisNo = false;
        raiz->ehDoisNo = true;

        if (p->apt1 == nullptr) {
          irmao->apt3 = p->apt2;

          if (p->apt2 != nullptr)
            p->apt2->pai = irmao;
        }
        else {
          irmao->apt3 = p->apt1;

          if (p->apt1 != nullptr)
            p->apt1->pai = irmao;
        }

        raiz->apt3 = nullptr;

        diminuiu = false;
        delete p;
      }
    }
  }
  else {
    No * p = destroy(raiz->apt2, chave, diminuiu);

    if (diminuiu) {
      No * irmao = raiz->apt1;

      if (!raiz->ehDoisNo) {
        if (!irmao->ehDoisNo) {
          p->chave1 = raiz->chave1;
          p->valor1 = raiz->valor1;
          raiz->chave1 = irmao->chave2;
          raiz->valor1 = irmao->valor2;
          irmao->ehDoisNo = true;

          if (p->apt1 == nullptr) {
            p->apt1 = irmao->apt3;
          }
          else {
            p->apt2 = p->apt1;
            p->apt1 = irmao->apt3;
          }

          if (irmao->apt3 != nullptr)
            irmao->apt3->pai = p;

          irmao->apt3 = nullptr;

          diminuiu = false;
        }
        else {
          irmao->chave2 = raiz->chave1;
          irmao->valor2 = raiz->valor1;
          irmao->ehDoisNo = false;
          raiz->chave1 = raiz->chave2;
          raiz->valor1 = raiz->valor2;
          raiz->apt2 = raiz->apt3;
          raiz->apt3 = nullptr;
          raiz->ehDoisNo = true;

          if (p->apt1 == nullptr) {
            irmao->apt3 = p->apt2;

            if (p->apt2 != nullptr)
              p->apt2->pai = irmao;
          }
          else {
            irmao->apt3 = p->apt1;

            if (p->apt1 != nullptr)
              p->apt1->pai = irmao;
          }

          delete p;
          diminuiu = false;
        }
      }
      else {
        if (!irmao->ehDoisNo) {
          p->chave1 = raiz->chave1;
          p->valor1 = raiz->valor1;
          raiz->chave1 = irmao->chave2;
          raiz->valor1 = irmao->valor2;
          irmao->ehDoisNo = true;

          if (p->apt1 == nullptr) {
            p->apt1 = irmao->apt3;
          }
          else {
            p->apt2 = p->apt1;
            p->apt1 = irmao->apt3;
          }

          if (irmao->apt3 != nullptr)
            irmao->apt3->pai = p;

          irmao->apt3 = nullptr;

          diminuiu = false;
        }
        else {
          // irmão pobre com pai dois-no
          irmao->chave2 = raiz->chave1;
          irmao->valor2 = raiz->valor1;
          irmao->ehDoisNo = false;

          // TEM QUE PEGAR OS PONTEIRO DA RAIZ E COLOCAR NO IRMAO
          if (p->apt1 == nullptr) {
            irmao->apt3 = p->apt2;

            if (p->apt2 != nullptr)
              p->apt2->pai = irmao;
          }
          else {
            irmao->apt3 = p->apt1;

            if (p->apt1 != nullptr)
              p->apt1->pai = irmao;
          }
          
          diminuiu = true;

          raiz->apt2 = nullptr; // desvincula p da raiz

          delete p;

          if (raiz->pai == nullptr) {
            delete raiz;
            raiz = irmao;
            raiz->pai = nullptr;
          }
        }
      }
    }
  }

  return raiz;
}

No * arvore23::max(No * raiz) {
  if (raiz == nullptr || raiz->apt2 == nullptr || (!raiz->ehDoisNo && raiz->apt3 == nullptr))
    return raiz;

  if (raiz->ehDoisNo)
    return max(raiz->apt2);
  else
    return max(raiz->apt3);
}

void arvore23::remove(Chave chave) {
  bool diminuiu = false;
  raiz = destroy(raiz, chave, diminuiu);
}

int arvore23::size(No * raiz) {
  if (raiz == nullptr)
    return 0;

  int esq = size(raiz->apt1);
  int meio = size(raiz->apt2);
  int dir = size(raiz->apt3);

  if (raiz->ehDoisNo)
    return esq + meio + 1;
  
  return esq + meio + dir + 2;
}

int arvore23::getRank(No * raiz, Chave chave) {
  if (raiz == nullptr)
    return NOT_FOUND_VALUE;

  if (raiz->chave1 == chave)
    return size(raiz->apt1);

  if (!raiz->ehDoisNo && raiz->chave2 == chave)
    return size(raiz->apt1) + 1 + size(raiz->apt2);

  if (chave < raiz->chave1)
    return getRank(raiz->apt1, chave);
  else if (!raiz->ehDoisNo && chave > raiz->chave2)
    return size(raiz->apt1) + size(raiz->apt2) + 2 + getRank(raiz->apt3, chave);
  else
    return size(raiz->apt1) + 1 + getRank(raiz->apt2, chave);
}

int arvore23::rank(Chave chave) {
  if (devolve(chave) == NOT_FOUND_VALUE)
    return NOT_FOUND_VALUE;
  
  return getRank(raiz, chave);
}

Chave arvore23::select(No * raiz, int k) {
  if (raiz == nullptr)
    return NOT_FOUND_KEY;

  int r = rank(raiz->chave1);

  if (r == k)
    return raiz->chave1;
  else if (r > k)
    return select(raiz->apt1, k);
  else if (raiz->ehDoisNo && r < k)
    return select(raiz->apt2, k);
  else {
    r = rank(raiz->chave2);

    if (r == k)
      return raiz->chave2;
    else if (r > k)
      return select(raiz->apt2, k);
    else
      return select(raiz->apt3, k);
  }
}

Chave arvore23::seleciona(int k) {
  return select(raiz, k);
}

void arvore23::printSt(No * raiz) {
  if (raiz == nullptr)
    return;

  printSt(raiz->apt1);
  cout << raiz->chave1 << ": " << raiz->valor1 << endl;
  printSt(raiz->apt2);

  if (!raiz->ehDoisNo) {
    cout << raiz->chave2 << ": " << raiz->valor2 << endl;
    printSt(raiz->apt3);
  }
}
