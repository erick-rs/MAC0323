#ifndef _NODE23_H
#define _NODE23_H

template <class Key, class Value>
class Node23 {
  public:
    Key chave1, chave2;
    Value valor1, valor2;
    Node23<Key, Value> * pai;
    Node23<Key, Value> * apt1;
    Node23<Key, Value> * apt2;
    Node23<Key, Value> * apt3;
    bool ehDoisNo;
    bool ehFolha();
    Node23();
    ~Node23();
};

template <class Key, class Value>
Node23<Key, Value>::Node23() {
  apt1 = apt2 = apt3 = nullptr;
}

template <class Key, class Value>
Node23<Key, Value>::~Node23() { }

template <class Key, class Value>
bool Node23<Key, Value>::ehFolha() {
  return (apt1 == nullptr) && (apt2 == nullptr) && (apt3 == nullptr);
}

#endif