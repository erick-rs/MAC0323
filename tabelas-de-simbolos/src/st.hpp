#ifndef _ST_H
#define _ST_H

#include "util.hpp"
#include <fstream>

template <class Key, class Value>
class ST {
  protected:
    virtual void printSt();
  
  public:
    ST();
    ~ST(); 
    virtual void insere(Key chave, Value valor);
    virtual Value devolve(Key chave);
    virtual void remove(Key chave);
    virtual int rank(Key chave);
    virtual Key seleciona(int k);
    void readFile(string nome_arquivo);
};

template <class Key, class Value>
ST<Key, Value>::ST() { }

template <class Key, class Value>
ST<Key, Value>::~ST() { }

template <class Key, class Value>
void ST<Key, Value>::insere(Key chave, Value valor) { }

template <class Key, class Value>
Value ST<Key, Value>::devolve(Key chave) {
  return NOT_FOUND_VALUE;
}

template <class Key, class Value>
void ST<Key, Value>::remove(Key chave) { }

template <class Key, class Value>
int ST<Key, Value>::rank(Key chave) {
  return NOT_FOUND_VALUE;
}

template <class Key, class Value>
Key ST<Key, Value>::seleciona(int k) {
  return NOT_FOUND_KEY;
}

template <class Key, class Value>
void ST<Key, Value>::printSt() { }

template <class Key, class Value>
void ST<Key, Value>::readFile(string nome_arquivo) {
  fstream arqTexto;
  string palavra;

  arqTexto.open(nome_arquivo.c_str());

  while (arqTexto >> palavra)
    insere(palavra, 1);

  arqTexto.close();
}

#endif
