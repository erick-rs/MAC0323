#ifndef _TREAPNODE_H
#define _TREAPNODE_H

template <class Key, class Value>
class TreapNode {
  public:
    TreapNode<Key, Value> * esq;
    TreapNode<Key, Value> * dir;
    Key chave;
    Value valor;
    int prior;
    TreapNode();
    ~TreapNode();
};

template <class Key, class Value>
TreapNode<Key, Value>::TreapNode() { }

template <class Key, class Value>
TreapNode<Key, Value>::~TreapNode() { }

#endif
