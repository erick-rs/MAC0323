#ifndef _NODERN_H
#define _NODERN_H

template <class Key, class Value>
class NodeRN {
  public:
    Key chave;
    Value valor;
    NodeRN<Key, Value> * pai;
    NodeRN<Key, Value> * esq;
    NodeRN<Key, Value> * dir;
    int cor;
    NodeRN();
    ~NodeRN();
};

template <class Key, class Value>
NodeRN<Key, Value>::NodeRN() { }

template <class Key, class Value>
NodeRN<Key, Value>::~NodeRN() { }

#endif
