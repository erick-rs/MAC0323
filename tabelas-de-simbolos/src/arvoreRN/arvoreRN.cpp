#include "arvoreRN.hpp"

typedef NodeRN<Chave, Valor> No;

arvoreRN::arvoreRN(string nome_arquivo) {
  raiz = nullptr;
  this->readFile(nome_arquivo);
  printSt(raiz);
}

arvoreRN::~arvoreRN() {
  deleteSt(raiz);
}

void arvoreRN::deleteSt(No * raiz) {
  if (raiz == nullptr)
    return;

  deleteSt(raiz->esq);
  deleteSt(raiz->dir);
  delete raiz;
}

No * arvoreRN::put(No * raiz, Chave chave, Valor valor) {
  if (raiz == nullptr) {
    raiz = new No();
    raiz->chave = chave;
    raiz->valor = valor;
    raiz->cor = VERMELHO;
    raiz->pai = nullptr;
    raiz->esq = nullptr;
    raiz->dir = nullptr;
  }
  else {
    No * p = raiz;
    bool achou = false;

    while (!achou) {
      if (chave < p->chave && p->esq != nullptr)
        p = p->esq;
      else if (chave < p->chave && p->esq == nullptr)
        achou = true;
      else if (chave > p->chave && p->dir != nullptr)
        p = p->dir;
      else if (chave > p->chave && p->dir == nullptr)
        achou = true;
      else
        achou = true;
    }

    if (p->chave == chave) {
      p->valor = p->valor + 1;
    }
    else {
      No * filho = new No();
      filho->chave = chave;
      filho->valor = valor;
      filho->cor = VERMELHO;
      filho->pai = p;

      if (chave < p->chave)
        p->esq = filho;
      else
        p->dir = filho;
      
      filho->esq = nullptr;
      filho->dir = nullptr;

      while (p != nullptr) {
        // p é pai de um nó vermelho
        if (p->cor == PRETO)
          break;
        else if (p->pai == nullptr) {
          p->cor = PRETO;
          break;
        }
        else {
          No * avo = p->pai;
          No * tio = (p->chave > avo->chave) ? avo->esq : avo->dir;

          if (tio != nullptr && tio->cor == VERMELHO) {
            avo->cor = VERMELHO;
            p->cor = PRETO;
            tio->cor = PRETO;
            // pois mudando o avo para vermelho, pode ter dado problema acima da subarvore
            p = avo->pai;
            filho = avo;
          }
          else {
            // tio é preto ou nullptr
            if (p == avo->esq && filho == p->esq) {
              // todos os nós do mesmo lado esquerdo, então rotaciona pra direita
              avo->esq = p->dir;
              if (p->dir != nullptr)
                p->dir->pai = avo;
              
              p->dir = avo;
              p->pai = avo->pai;
              if (avo->pai != nullptr) {
                if (avo->pai->dir == avo)
                  avo->pai->dir = p;
                else
                  avo->pai->esq = p;
              }
              avo->pai = p;
              
              p->cor = PRETO;
              avo->cor = VERMELHO;

              if (avo == raiz)
                raiz = p;
              
              break;
            }
            else if (p->dir == filho && avo->esq == p) {
              // rotação dupla: primeiro da esquerda e depois da direita
              
              // primeira rotação
              p->dir = filho->esq;
              if (filho->esq != nullptr)
                filho->esq->pai = p;

              filho->esq = p;
              p->pai = filho;
              filho->pai = avo;
              avo->esq = filho;

              // agora com essas duas linhas, a próxima iteração vai cair no primeiro if
              p = filho;
              filho = filho->esq;
            }
            else if (p == avo->dir && filho == p->dir) {
              // todos os nós do mesmo lado direito, então rotaciona pra esquerda
              avo->dir = p->esq;
              if (p->esq != nullptr)
                p->esq->pai = avo;

              p->esq = avo;
              p->pai = avo->pai;
              if (avo->pai != nullptr) {
                if (avo->pai->dir == avo)
                  avo->pai->dir = p;
                else
                  avo->pai->esq = p;
              }

              avo->pai = p;

              p->cor = PRETO;
              avo->cor = VERMELHO;

              if (avo == raiz)
                raiz = p;

              break;
            }
            else {
              // rotação dupla: primeiro da direita e depois da esquerda

              // primeira rotação
              p->esq = filho->dir;
              if (filho->dir != nullptr)
                filho->dir->pai = p;

              filho->dir = p;
              p->pai = filho;
              filho->pai = avo;
              avo->dir = filho;

              // agora com essas duas linhas abaixo, a próxima iteração vai cair no if de cima
              p = filho;
              filho = filho->dir;
            }
          }
        }
      }
    }
  }

  return raiz;
}

void arvoreRN::insere(Chave chave, Valor valor) {
  raiz = put(raiz, chave, valor);
}

Valor arvoreRN::get(No * raiz, Chave chave) {
  if (raiz == nullptr)
    return NOT_FOUND_VALUE;

  if (chave == raiz->chave)
    return raiz->valor;
  else if (chave < raiz->chave)
    return get(raiz->esq, chave);
  else
    return get(raiz->dir, chave);
}

Valor arvoreRN::devolve(Chave chave) {
  return get(raiz, chave);
}

No * arvoreRN::destroy(No * raiz, Chave chave) {
  if (raiz == nullptr)
    return raiz;
  else {
    No * p = raiz;
    bool achou = false;

    while (!achou) {
      if (chave < p->chave)
        p = p->esq;
      else if (chave > p->chave)
        p = p->dir;
      else {
        if (p->esq == nullptr && p->dir == nullptr) {
          achou = true;
        }
        else if (p->esq != nullptr) {
          No * aux = max(p->esq);
          p->chave = aux->chave;
          p->valor = aux->valor;
          chave = aux->chave;
          p = p->esq;
        }
        else {
          No * aux = min(p->dir);
          p->chave = aux->chave;
          p->valor = aux->valor;
          chave = aux->chave;
          p = p->dir;
        }
      }
    }

    if (achou) {
      if (p == raiz) {
        delete p;
        raiz = nullptr;
      }
      else if (p->cor == VERMELHO) {
        // caso 1.0: folha vermelha
        if (p->pai->esq == p)
          p->pai->esq = nullptr;
        else
          p->pai->dir = nullptr;
        
        delete p;
      }
      else {
        /*
          guardando o nó que eu quero remover, pq senão ele pode passar varias vezes pelos casos em que eu
          removo de fato o nó (casos 2.1 e 2.4), assim ele poderia remover alguem que não é o que eu queria
        */
        No * aux = p;

        while (true) {
          if (p->cor == DUPLOPRETO && p->pai == nullptr) {
            // caso 2.0: duplo preto é a raiz
            p->cor = PRETO;
            break;
          }
          else {
            No * pai = p->pai;
            No * irmao = (pai->chave > p->chave) ? pai->dir : pai->esq;
            
            if (irmao != nullptr && irmao->cor == VERMELHO) {
              // caso 2.2: irmão vermelho
              /* troca a cor do pai com o irmão e 
                rotaciona na direção do duplo preto (se ele tiver na esquerda, rotaciona pra la)
              */
              int auxCor = irmao->cor;
              irmao->cor = pai->cor;
              pai->cor = auxCor;

              p->cor = DUPLOPRETO;

              if (pai->esq == p) {
                // rotaciona pra esquerda
                // NÃO ESQUECER DE ATUALIZAR OS PAIS
                irmao->pai = pai->pai; // avo

                if (pai->pai != nullptr) {
                  if (pai->pai->esq == pai)
                    pai->pai->esq = irmao;
                  else
                    pai->pai->dir = irmao;
                }
                else {
                  // quer dizer que o irmão vai ser a nova raiz;
                  raiz = irmao;
                }

                pai->dir = irmao->esq;

                if (irmao->esq != nullptr)
                  irmao->esq->pai = pai;

                irmao->esq = pai;
                pai->pai = irmao;
              }
              else {
                // rotaciona pra direita
                irmao->pai = pai->pai;

                if (pai->pai != nullptr) {
                  if (pai->pai->esq == pai)
                    pai->pai->esq = irmao;
                  else
                    pai->pai->dir = irmao;
                }
                else {
                  // quer dizer que o irmão vai ser a nova raiz;
                  raiz = irmao;
                }

                pai->esq = irmao->dir;

                if (irmao->dir != nullptr)
                  irmao->dir->pai = pai;

                irmao->dir = pai;
                pai->pai = irmao;
              }
            }
            else if (irmao != nullptr && irmao->cor == PRETO) {
              if ((irmao->esq == nullptr || irmao->esq->cor == PRETO) && 
                  (irmao->dir == nullptr || irmao->dir->cor == PRETO)) 
              {
                // caso 2.1: irmão preto com filhos pretos
                irmao->cor = VERMELHO;

                if (p == aux) {
                  if (pai->esq == p)
                    pai->esq = nullptr;
                  else
                    pai->dir = nullptr;
                  
                  delete p;
                }

                if (pai->cor == VERMELHO) {
                  pai->cor = PRETO;
                  break;
                }
                else {
                  pai->cor = DUPLOPRETO;
                  p = pai; // empurrei o problema pra cima
                }
              }
              else {
                No * sobrinhoPerto = (pai->esq == p) ? irmao->esq : irmao->dir;
                No * sobrinhoLonge = (pai->esq == p) ? irmao->dir : irmao->esq;

                if (sobrinhoLonge != nullptr && sobrinhoLonge->cor == VERMELHO) {
                  /* 
                    caso 2.4: troca a cor do pai com o irmão,
                    rotaciona a subarvore com o pai do duplo preto na direção do duplo preto
                    e pinta o antigo sobrinho que era vermelho de preto
                  */
                  int auxCor = irmao->cor;
                  irmao->cor = pai->cor;
                  pai->cor = auxCor;

                  if (pai->esq == p) {
                    // rotaciona pra esquerda
                    irmao->pai = pai->pai;

                    if (pai->pai != nullptr) {
                      if (pai->pai->esq == pai)
                        pai->pai->esq = irmao;
                      else
                        pai->pai->dir = irmao;
                    }
                    else {
                      // quer dizer que o irmão vai ser a nova raiz;
                      raiz = irmao;
                    }

                    pai->dir = irmao->esq;
                    
                    if (irmao->esq != nullptr)
                      irmao->esq->pai = pai;

                    irmao->esq = pai;
                    pai->pai = irmao;
                  }
                  else {
                    // rotaciona pra direita
                    irmao->pai = pai->pai;

                    if (pai->pai != nullptr) {
                      if (pai->pai->esq == pai)
                        pai->pai->esq = irmao;
                      else
                        pai->pai->dir = irmao;
                    }
                    else {
                      // quer dizer que o irmão vai ser a nova raiz;
                      raiz = irmao;
                    }

                    pai->esq = irmao->dir;

                    if (irmao->dir != nullptr)
                      irmao->dir->pai = pai;

                    irmao->dir = pai;
                    pai->pai = irmao;
                  }

                  sobrinhoLonge->cor = PRETO;

                  if (p == aux) {
                    if (pai->esq == p)
                      pai->esq = nullptr;
                    else
                      pai->dir = nullptr;
                    
                    delete p;
                  }
                  else
                    p->cor = PRETO;

                  break;
                }

                if (sobrinhoPerto != nullptr && sobrinhoPerto->cor == VERMELHO) {
                  /* 
                    caso 2.3: troca a cor do irmão e desse filho e
                    rotaciona a subarvore do irmão na direção contrária do duplo preto 
                    (se ele tá na direita, rotaciona pra esquerda. Se ele tá na esquerda, rotaciona pra direita)
                  */
                  int auxCor = irmao->cor;
                  irmao->cor = sobrinhoPerto->cor;
                  sobrinhoPerto->cor = auxCor;

                  if (pai->esq == p) {
                    // rotaciona pra direita (contrário da onde o p tá)
                    sobrinhoPerto->pai = irmao->pai;
                    irmao->pai->dir = sobrinhoPerto;

                    irmao->esq = sobrinhoPerto->dir;

                    if (sobrinhoPerto->dir != nullptr)
                      sobrinhoPerto->dir->pai = irmao;

                    sobrinhoPerto->dir = irmao;
                    irmao->pai = sobrinhoPerto;
                  }
                  else {
                    // rotaciona pra esq (contrário da onde o p tá)
                    sobrinhoPerto->pai = pai;
                    pai->esq = sobrinhoPerto;

                    irmao->dir = sobrinhoPerto->esq;

                    if (sobrinhoPerto->esq != nullptr)
                      sobrinhoPerto->esq->pai = irmao;

                    sobrinhoPerto->esq = irmao;
                    irmao->pai = sobrinhoPerto;
                  }
                }
              }
            }
          }
        }
      }
    }

    return raiz;
  }
}

No * arvoreRN::max(No * raiz) {
  if (raiz == nullptr || raiz->dir == nullptr)
    return raiz;

  return max(raiz->dir);
}

No * arvoreRN::min(No * raiz) {
  if (raiz == nullptr || raiz->esq == nullptr)
    return raiz;

  return min(raiz->esq);
}

void arvoreRN::remove(Chave chave) {
  raiz = destroy(raiz, chave);
}

int arvoreRN::size(No * raiz) {
  if (raiz == nullptr)
    return 0;

  int esq = size(raiz->esq);
  int dir = size(raiz->dir);
  return esq + dir + 1;
}

int arvoreRN::getRank(No * raiz, Chave chave) {
  if (raiz == nullptr)
    return NOT_FOUND_VALUE;
    
  if (chave < raiz->chave)
    return getRank(raiz->esq, chave);
  else if (chave > raiz->chave)
    return size(raiz->esq) + 1 + getRank(raiz->dir, chave);
  else
    return size(raiz->esq);
}

int arvoreRN::rank(Chave chave) {
  if (devolve(chave) == NOT_FOUND_VALUE)
    return NOT_FOUND_VALUE;
  
  return getRank(raiz, chave);
}

Chave arvoreRN::select(No * raiz, int k) {
  if (raiz == nullptr)
    return NOT_FOUND_KEY;

  int r = rank(raiz->chave);
  
  if (r == k)
    return raiz->chave;
  else if (r > k)
    return select(raiz->esq, k);
  else
    return select(raiz->dir, k);
}

Chave arvoreRN::seleciona(int k) {
  return select(raiz, k);
}

void arvoreRN::printSt(No * raiz) {
  if (raiz == nullptr)
    return;

  printSt(raiz->esq);
  cout << raiz->chave << ": " << raiz->valor << endl;
  printSt(raiz->dir);
}
