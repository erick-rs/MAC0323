#ifndef _ARVORERN_H
#define _ARVORERN_H

#include "../st.hpp"
#include "../nodeRN.hpp"

enum COR {
  VERMELHO,
  PRETO,
  DUPLOPRETO
};

class arvoreRN : public ST<Chave, Valor> {
  private:
    NodeRN<Chave, Valor> * raiz;
    NodeRN<Chave, Valor> * put(NodeRN<Chave, Valor> * raiz, Chave chave, Valor valor);
    NodeRN<Chave, Valor> * destroy(NodeRN<Chave, Valor> * raiz, Chave chave);
    NodeRN<Chave, Valor> * max(NodeRN<Chave, Valor> * raiz);
    NodeRN<Chave, Valor> * min(NodeRN<Chave, Valor> * raiz);
    Valor get(NodeRN<Chave, Valor> * raiz, Chave chave);
    int size(NodeRN<Chave, Valor> * raiz);
    int getRank(NodeRN<Chave, Valor> * raiz, Chave chave);
    Chave select(NodeRN<Chave, Valor> * raiz, int k);
    void deleteSt(NodeRN<Chave, Valor> * raiz);
    void printSt(NodeRN<Chave, Valor> * raiz);
  
  public:
    arvoreRN(string nome_arquivo);
    virtual ~arvoreRN();
    void insere(Chave chave, Valor valor) override;
    Valor devolve(Chave chave) override;
    void remove(Chave chave) override;
    int rank(Chave chave) override;
    Chave seleciona(int k) override;
};


#endif
