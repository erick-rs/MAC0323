#ifndef _TREENODE_H
#define _TREENODE_H

template <class Key, class Value>
class TreeNode {
  public:
    TreeNode<Key, Value> * esq;
    TreeNode<Key, Value> * dir;
    Key chave;
    Value valor;
    int size;
    TreeNode();
    ~TreeNode();
};

template <class Key, class Value>
TreeNode<Key, Value>::TreeNode() { }

template <class Key, class Value>
TreeNode<Key, Value>::~TreeNode() { }

#endif
