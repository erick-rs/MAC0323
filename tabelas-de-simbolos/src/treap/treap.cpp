#include "treap.hpp"

typedef TreapNode<Chave, Valor> No;

treap::treap(string nome_arquivo) {
  raiz = nullptr;
  this->readFile(nome_arquivo);
  printSt(raiz);
}

treap::~treap() {
  deleteSt(raiz);
}

void treap::deleteSt(No * raiz) {
  if (raiz == nullptr)
    return;

  deleteSt(raiz->esq);
  deleteSt(raiz->dir);
  delete raiz;
}

No * treap::newNo(Chave chave, Valor valor) {
  No * treap = new No();
  treap->chave = chave;
  treap->valor = valor;
  treap->prior = rand();
  treap->esq = nullptr;
  treap->dir = nullptr;

  return treap;
}

No * treap::rotacionaEsquerda(No * raiz) {
  No * novaRaiz = raiz->dir;
  No * no = novaRaiz->esq;

  novaRaiz->esq = raiz;
  raiz->dir = no;

  return novaRaiz;
}

No * treap::rotacionaDireita(No * raiz) {
  No * novaRaiz = raiz->esq;
  No * no = novaRaiz->dir;

  novaRaiz->dir = raiz;
  raiz->esq = no;

  return novaRaiz;
}

No * treap::put(No * raiz, Chave chave, Valor valor) {
  if (raiz == nullptr)
    return newNo(chave, valor);

  if (chave == raiz->chave) {
    raiz->valor = raiz->valor + 1;
  }
  else if (chave < raiz->chave) {
    raiz->esq = put(raiz->esq, chave, valor);

    if (raiz->esq != nullptr && raiz->esq->prior > raiz->prior)
      raiz = rotacionaDireita(raiz);
  }
  else {
    raiz->dir = put(raiz->dir, chave, valor);

    if (raiz->dir != nullptr && raiz->dir->prior > raiz->prior)
      raiz = rotacionaEsquerda(raiz);
  }

  return raiz;
}

void treap::insere(Chave chave, Valor valor) {
  raiz = put(raiz, chave, valor);
}

Valor treap::get(No * raiz, Chave chave) {
  if (raiz == nullptr)
    return NOT_FOUND_VALUE;

  if (chave == raiz->chave)
    return raiz->valor;
  else if (chave < raiz->chave)
    return get(raiz->esq, chave);
  else
    return get(raiz->dir, chave);
}

Valor treap::devolve(Chave chave) {
  return get(raiz, chave);
}

No * treap::destroy(No * raiz, Chave chave) {
  if (raiz == nullptr)
    return raiz;

  if (chave < raiz->chave)
    raiz->esq = destroy(raiz->esq, chave);
  else if (chave > raiz->chave)
    raiz->dir = destroy(raiz->dir, chave);
  else {
    if (raiz->esq == nullptr && raiz->dir == nullptr) {
      delete raiz;
      raiz = nullptr;
    }
    else if (raiz->esq != nullptr && raiz->dir != nullptr) {
      if (raiz->esq->prior < raiz->dir->prior) {
        raiz = rotacionaEsquerda(raiz);
        raiz->esq = destroy(raiz->esq, chave);
      }
      else {
        raiz = rotacionaDireita(raiz);
        raiz->dir = destroy(raiz->dir, chave);
      }
    }
    else {
      No * novaRaiz;

      if (raiz->esq == nullptr)
        novaRaiz = raiz->dir;
      else
        novaRaiz = raiz->esq;
      
      delete raiz;
      raiz = novaRaiz;
    }
  }
  
  return raiz;
}

void treap::remove(Chave chave) {
  raiz = destroy(raiz, chave);
}

int treap::size(No * raiz) {
  if (raiz == nullptr)
    return 0;

  int esq = size(raiz->esq);
  int dir = size(raiz->dir);
  return esq + dir + 1;
}

int treap::getRank(No * raiz, Chave chave) {
  if (raiz == nullptr)
    return 0;
  
  if (chave < raiz->chave)
    return getRank(raiz->esq, chave);
  else if (chave > raiz->chave)
    return size(raiz->esq) + 1 + getRank(raiz->dir, chave);
  else
    return size(raiz->esq);
}

int treap::rank(Chave chave) {
  if (devolve(chave) == NOT_FOUND_VALUE)
    return NOT_FOUND_VALUE;

  return getRank(raiz, chave);
}

Chave treap::select(No * raiz, int k) {
  while (raiz == nullptr)
    return NOT_FOUND_KEY;

  int r = rank(raiz->chave);

  if (r == k)
    return raiz->chave;
  else if (r > k)
    return select(raiz->esq, k);
  else
    return select(raiz->dir, k);
}

Chave treap::seleciona(int k) {
  return select(raiz, k);
}

void treap::printSt(No * raiz) {
  if (raiz == nullptr)
    return;

  printSt(raiz->esq);
  cout << raiz->chave << endl;
  printSt(raiz->dir);
}
