#ifndef _TREAP_H
#define _TREAP_H

#include "../st.hpp"
#include "../treapNode.hpp"

class treap : public ST<Chave, Valor> {
  private:
    TreapNode<Chave, Valor> * raiz;
    TreapNode<Chave, Valor> * newNo(Chave chave, Valor valor);
    TreapNode<Chave, Valor> * put(TreapNode<Chave, Valor> * raiz, Chave chave, Valor valor);
    TreapNode<Chave, Valor> * destroy(TreapNode<Chave, Valor> * raiz, Chave chave);
    TreapNode<Chave, Valor> * rotacionaEsquerda(TreapNode<Chave, Valor> * raiz);
    TreapNode<Chave, Valor> * rotacionaDireita(TreapNode<Chave, Valor> * raiz);
    Valor get(TreapNode<Chave, Valor> * raiz, Chave chave);
    Chave select(TreapNode<Chave, Valor> * raiz, int k);
    int getRank(TreapNode<Chave, Valor> * raiz, Chave chave);
    int size(TreapNode<Chave, Valor> * raiz);
    void deleteSt(TreapNode<Chave, Valor> * raiz);

  protected:
    void printSt(TreapNode<Chave, Valor> * raiz);

  public:
    treap(string nome_arquivo);
    virtual ~treap();
    void insere(Chave chave, Valor valor) override;
    Valor devolve(Chave chave) override;
    void remove(Chave chave) override;
    int rank(Chave chave) override;
    Chave seleciona(int k) override;
};

#endif
