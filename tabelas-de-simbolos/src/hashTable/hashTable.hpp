#ifndef _HASHTABLE_H
#define _HASHTABLE_H

#define M 4000

#include "../st.hpp"
#include "../node.hpp"
#include <vector>

class hashTable : public ST<Chave, Valor> {
  private:
    vector<Node<Chave, Valor> *> st;
    int hash(Chave chave);

  protected:
    void printSt();
  
  public:
    hashTable(string nome_arquivo);
    virtual ~hashTable();
    void insere(Chave chave, Valor valor) override;
    Valor devolve(Chave chave) override;
    void remove(Chave chave) override;
    int rank(Chave chave) override;
    Chave seleciona(int k) override;
};

#endif
