#include "hashTable.hpp"

typedef Node<Chave, Valor> No;

hashTable::hashTable(string nome_arquivo) {
  st = vector<No *>(M);
  for (unsigned int i = 0; i < st.size(); i++)
    st[i] = nullptr;
  
  this->readFile(nome_arquivo);
  printSt();
}

hashTable::~hashTable() {
  for (unsigned int i = 0; i < st.size(); i++) {
    No * first = st[i];

    while (first != nullptr) {
      No * node = first;
      first = first->next;
      delete node;
    }
  }
}

void hashTable::insere(Chave chave, Valor valor) {
  int h = hash(chave);
  No * node = st[h];

  while (node != nullptr && node->chave != chave)
    node = node->next;

  if (node == nullptr) {
    No * newNode = new No();
    newNode->chave = chave;
    newNode->valor = valor;
    newNode->next = st[h];
    st[h] = newNode;
  }
  else {
    node->valor = node->valor + 1;
  }
}

int hashTable::hash(Chave chave) {
  unsigned hashVal = 0;

  for (unsigned int i = 0; i < chave.length(); i++) {
    hashVal = (hashVal * 23  + chave[i]) % M;
  }

  return hashVal;
}

Valor hashTable::devolve(Chave chave) {
  int h = hash(chave);
  No * node = st[h];

  while (node != nullptr) {
    if (node->chave == chave)
      return node->valor;
    
    node = node->next;
  }
  
  return NOT_FOUND_VALUE;
}

void hashTable::remove(Chave chave) {
  int h = hash(chave);
  No * node = st[h];
  No * anterior = nullptr;

  while (node != nullptr && node->chave != chave) {
    anterior = node;
    node = node->next;
  }

  if (node != nullptr && node->chave == chave) {
    if (anterior == nullptr)
      st[h] = st[h]->next;
    else
      anterior->next = node->next;

    delete node;
  }
}

int hashTable::rank(Chave chave) {
  int rank = 0;

  for (unsigned int i = 0; i < st.size(); i++) {
    No * node = st[i];

    while (node != nullptr) {
      if (node->chave < chave)
        rank += 1;

      node = node->next;
    }
  }

  return rank;
}

Chave hashTable::seleciona(int k) {
  for (unsigned int i = 0; i < st.size(); i++) {
    No * node = st[i];

    while (node != nullptr) {
      if (rank(node->chave) == k)
        return node->chave;
      
      node = node->next;
    }
  }
  
  return NOT_FOUND_KEY;
}

void hashTable::printSt() {
  for (unsigned int i = 0; i < st.size(); i++) {
    No * node = st[i];

    while (node != nullptr) {
      cout << node->chave << ": " << node->valor << endl;
      node = node->next;
    }
  }
}
