#include "vetorDes.hpp"

vetorDes::vetorDes(string nome_arquivo) {
  st = new Par*[10];
  n = 0;
  size = 10;
  this->readFile(nome_arquivo);
  printSt();
}

vetorDes::~vetorDes() {
  for (int i = 0; i < n; i++)
    delete st[i];
  
  delete [] st;
}

void vetorDes::insere(Chave chave, Valor valor) {
  if (n == size)
    resize();
  
  for (int i = 0; i < n; i++) {
    Chave ch = st[i]->chave;

    if (chave == ch) {
      st[i]->valor = st[i]->valor + 1;
      return;
    }
  }

  st[n++] = new Par(chave, valor);
}

Valor vetorDes::devolve(Chave chave) {
  for (int i = 0; i < n; i++) {
    Chave ch = st[i]->chave;

    if (chave == ch)
      return st[i]->valor;
  }

  return NOT_FOUND_VALUE;
}

void vetorDes::remove(Chave chave) {
  for (int i = 0; i < n; i++) {
    Chave ch = st[i]->chave;

    if (chave == ch) {
      delete st[i];
      
      for (int j = i+1; j < n; j++)
        st[j-1] = st[j];
      
      st[n-1] = nullptr;
      
      n -= 1;
      return;
    }
  }
}

int vetorDes::rank(Chave chave) {
  int rank = 0;

  if (devolve(chave) == NOT_FOUND_VALUE)
    return NOT_FOUND_VALUE;

  for (int i = 0; i < n; i++) {
    Chave ch = st[i]->chave;

    if (chave > ch)
      rank += 1;
  }

  return rank;
}

Chave vetorDes::seleciona(int k) {
  for (int i = 0; i < n; i++) {
    Chave ch = st[i]->chave;

    if (rank(ch) == k)
      return ch;
  }

  return NOT_FOUND_KEY;
}

void vetorDes::resize() {
  Par ** newst = new Par*[2 * n];

  for (int i = 0; i < n; i++) {
    newst[i] = st[i];
    st[i] = nullptr;
  }

  delete [] st;
  st = newst;
  size = 2 * n;
}

void vetorDes::printSt() {
  for (int i = 0; i < n; i++)
    cout << st[i]->chave << endl;
}
