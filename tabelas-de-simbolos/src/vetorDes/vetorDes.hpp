#ifndef _VETORDES_H
#define _VETORDES_H

#include "../st.hpp"
#include "../pair.hpp"

typedef Pair<Chave, Valor> Par;

class vetorDes : public ST<Chave, Valor> {
  private:
    Par ** st;
    int n;
    int size;
    void resize();

  protected:
    void printSt() override;

  public:
    vetorDes(string nome_arquivo);
    virtual ~vetorDes();
    void insere(Chave chave, Valor valor) override;
    Valor devolve(Chave chave) override;
    void remove(Chave chave) override;
    int rank(Chave chave) override;
    Chave seleciona(int k) override;
};

#endif
