#include "listaOrd.hpp"

typedef Node<Chave, Valor> No;

listaOrd::listaOrd(string nome_arquivo) {
  first = nullptr;
  this->readFile(nome_arquivo);
  printSt();
}

listaOrd::~listaOrd() {
  while (first != nullptr) {
    No * node = first;
    first = first->next;
    delete node;
  }
}

void listaOrd::insere(Chave chave, Valor valor) {
  if (first == nullptr) {
    first = new No();
    first->chave = chave;
    first->valor = valor;
    first->next = nullptr;
  }
  else {
    No * node = first;
    No * anterior = nullptr;

    while (node != nullptr && chave > node->chave) {
      anterior = node;
      node = node->next;
    }

    if (node != nullptr && chave == node->chave) {
      node->valor = node->valor + 1;
    }
    else {
      No * newNode = new No();
      newNode->chave = chave;
      newNode->valor = valor;

      if (node == nullptr) {
        anterior->next = newNode;
        newNode->next = nullptr;
      }
      else {
        if (anterior == nullptr) {
          newNode->next = first;
          first = newNode;
        }
        else {
          newNode->next = node;
          anterior->next = newNode;
        }
      }
    }
  }
}

Valor listaOrd::devolve(Chave chave) {
  No * node = first;

  while (node != nullptr) {
    Chave ch = node->chave;

    if (chave == ch)
      return node->valor;
    
    node = node->next;
  }

  return NOT_FOUND_VALUE;
}

void listaOrd::remove(Chave chave) {
  No * node = first;
  No * anterior = nullptr;

  while (node != nullptr) {
    if (chave == node->chave) {
      if (anterior == nullptr) {
        anterior = node;
        node = node->next;
        first = node;
        delete anterior;
      }
      else {
        anterior->next = node->next;
        node->next = nullptr;
        delete node;
      }

      return;
    }

    anterior = node;
    node = node->next;
  }
}

int listaOrd::rank(Chave chave) {
  No * node = first;
  int rank = 0;

  if (devolve(chave) == NOT_FOUND_VALUE)
    return NOT_FOUND_VALUE;

  while (node != nullptr) {
    Chave ch = node->chave;

    if (chave == ch)
      break;
    else
      rank += 1;

    node = node->next;
  }

  return rank;
}

Chave listaOrd::seleciona(int k) {
  No * node = first;
  
  while (node != nullptr && k > 0) {
    node = node->next;
    k -= 1;
  }

  return node != nullptr ? node->chave : NOT_FOUND_KEY;
}

void listaOrd::printSt() {
  No * node = first;

  while (node != nullptr) {
    cout << node->chave << endl;
    node = node->next;
  }
}
