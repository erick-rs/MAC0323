#ifndef _LISTAORD_H
#define _LISTAORD_H

#include "../st.hpp"
#include "../node.hpp"

class listaOrd : public ST<Chave, Valor> {
  private:
    Node<Chave, Valor> * first;

  protected:
    void printSt() override;
  
  public:
    listaOrd(string nome_arquivo);
    virtual ~listaOrd();
    void insere(Chave chave, Valor valor) override;
    Valor devolve(Chave chave) override;
    void remove(Chave chave) override;
    int rank(Chave chave) override;
    Chave seleciona(int k) override;
};

#endif
