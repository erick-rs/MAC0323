#ifndef _PAIR_H
#define _PAIR_H

template <class Key, class Value>
class Pair {
  public:
    Key chave;
    Value valor;
    Pair();
    Pair(Key chave, Value valor);
    ~Pair();
};

template <class Key, class Value>
Pair<Key, Value>::Pair() { }

template <class Key, class Value>
Pair<Key, Value>::Pair(Key chave, Value valor) {
  this->chave = chave;
  this->valor = valor;
}

template <class Key, class Value>
Pair<Key, Value>::~Pair() { }

#endif
