# Tabelas de símbolos
Programa que calcula a frequência de palavras em um texto. O objetivo é testar diferentes
diferentes implementações de uma tabela de símbolos ordenada, visando observar empiricamente cada
uma dessas implementações com relação a complexidade de tempo de execução.
Esse programa foi feito para a disciplina MAC0323 - Algoritmos e Estruturas de Dados II.  

## Estruturas de dados usadas
- Vetor desordenado (VD)
- Vetor ordenado (VO)
- Lista ligada desordenada (LD)
- Lista ligada ordenada (LO)
- Árvore Binária (AB)
- Treaps (TR)
- Árvore 2-3 (A23)
- Árvore Rubro-Negra (RN)
- Hashing Table (HS)

## Executando o programa
Para executar o programa, basta executar dois comandos:

1. make
2. ./ep1 tests/nome_do_arquivo #modo#

OBS.: a pasta tests contém arquivos .txt usados para criar as tabelas de símbolos.
OBS.: em #modo#, coloque alguma das nove siglas utilizadas no tópico "Estruturas de dados usadas".
