
#include <bits/stdc++.h>

bool validateStackSequences(vector<int>& pushed, vector<int>& popped) {
    bool valid = true;
    stack<int> st;
    int pos = 0;
    for (int i = 0; i < pushed.size() && valid; i++) {
        if (popped[i] == pushed[pos]) {
            pos++;
        }
        else if (popped[i] == st.top()) {
            st.pop();
        }
        else {
            while (pos < pushed.size()) {
                st.push(pushed[pos])
                if (st.top() == popped[i]) {
                    st.pop();
                    break;
                }
                pos++;
            }
            if (pos == pushed.size())
                valid = false;
        }
    }
    return valid;
}


int main () {
    int n;
    cin >> n;
    vector<int> pushed;
    for (int i = 0; i < n; i++)
        pushed.push(n);
    validateStackSequences(pushed);
}