#include "RegularExpression.hpp"
#include <iostream>
using namespace std;
#include <stack>

RegularExpression::RegularExpression(string regex) {
  this->regex = regex;
  this->graph = new Graph(regex.length());
  this->buildGraph();
}

RegularExpression::~RegularExpression() {
  delete this->graph;
}

void RegularExpression::buildGraph() {
  /* Build a Graph with the regex */
  stack<pair<int, char>> stack;

  for (int i = 0; i < (int) regex.length(); i++) {
    int previous = i;

    if (regex[i] == '\\') {
      graph->addArrow(i, i + 1);
      i += 1;
    }
    else if (regex[i] == '(' || regex[i] == '|')
      stack.push(make_pair(i, regex[i]));
    else if (regex[i] == ')') {
      pair<int, char> topOp = stack.top();
      stack.pop();

      if (topOp.second == '|') {
        // stack pop while top is '|'
        // adding arrow from each '|' to ')'
        // when stack top is '(', add arrow from '(' to each '|'
        vector<int> orCharacters;

        while (!stack.empty() && topOp.second == '|') {
          graph->addArrow(topOp.first, i); // add arrow from '|' to ')'
          orCharacters.push_back(topOp.first);
          topOp = stack.top();
          stack.pop();
        }

        previous = topOp.first; // is '(' of ')'

        for (int j = 0; j < (int) orCharacters.size(); j++)
          graph->addArrow(previous, orCharacters[j] + 1); // add arrow from '(' to character after '|'
      }
      else
        previous = topOp.first;
    }

    if (i + 1 < (int) regex.length() && regex[i + 1] == '*') {
      graph->addArrow(previous, i + 1);
      graph->addArrow(i + 1, previous);
    }

    if ((i - 1 < 0 || regex[i - 1] != '\\') && (regex[i] == '*' || regex[i] == '(' || regex[i] == ')'))
      graph->addArrow(i, i + 1);
  }
}

bool RegularExpression::match(string word) {
  vector<bool> matches(graph->vertices() + 1, false);

  graph->dfs(0, matches);

  for (int i = 0; i < (int) word.length(); i++) {
    // test the character
    vector<bool> currentMatches(graph->vertices() + 1, false);

    for (int j = 0; j < graph->vertices(); j++)
      if (matches[j] && (regex[j] == word[i] || ((j - 1 < 0 || regex[j - 1] != '\\') && regex[j] == '.')))
        currentMatches[j + 1] = true;

    vector<bool> marked(graph->vertices() + 1);

    for (int j = 0; j < (int) matches.size(); j++)
      matches[j] = false;

    for (int j = 0; j < (int) matches.size(); j++) {
      if (currentMatches[j]) {
        for (int k = 0; k < (int) matches.size(); k++)
          marked[k] = false;

        graph->dfs(j, marked);

        for (int k = 0; k < (int) matches.size(); k++)
          if (marked[k])
            matches[k] = true;
      }
    }
  }

  if (matches[graph->vertices()])
    return true;

  return false;
}
