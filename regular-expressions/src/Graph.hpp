#ifndef _GRAPH_H
#define _GRAPH_H

#include <vector>
using namespace std;

class Graph {
  private:
    vector<vector<int>> graph;
    int V;

  public:
    Graph(int vertices);
    ~Graph();
    int vertices();
    void addArrow(int u, int v);
    void dfs(int v, vector<bool> & marked);
    void showGraph();
};

#endif