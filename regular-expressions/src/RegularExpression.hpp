#ifndef _REGULAR_EXPRESSION_H
#define _REGULAR_EXPRESSION_H

#include <string>
#include "Graph.hpp"
using namespace std;

class RegularExpression {
  private:
    string regex;
    Graph * graph;
    void buildGraph();

  public:
    RegularExpression(string regex);
    ~RegularExpression();
    bool match(string word);
};

#endif
