#include <iostream>
#include "RegularExpression.hpp"
#include "util.hpp"
using namespace std;

#define PROMPT cout << ">>> ";

int main() {
  RegularExpression * regularExpression;
  string regex;
  string word = "";
  vector<string> possibleResults = {"False", "True"};

  cout << "Digite a expressão regular: ";
  cin >> regex;

  regex = treatRegularExpression(regex);

  cout << "REGEX FINAL: " << regex << endl;

  regularExpression = new RegularExpression(regex);

  cout << "Agora digite as palavras que gostaria de testar (Pressione CTRL + D para sair do programa): " << endl;

  PROMPT;

  while (cin >> word) {
    cout << possibleResults[regularExpression->match(word)] << endl;
    PROMPT;
  }

  cout << endl;

  delete regularExpression;

  return EXIT_SUCCESS;
}
