#include "util.hpp"
#include <vector>
#include <iostream>
#include <stack>

static string interval(string regex);
static string set(string regex);
static string oneOrMore(string regex);
static string complement(string regex);

string treatRegularExpression(string regex) {
  regex = complement(regex);
  regex = interval(regex);
  regex = set(regex);
  regex = oneOrMore(regex);

  return regex;
}

static string interval(string regex) {
  string newRegex;
  int n = regex.length();

  for (int i = 0; i < n; i++) {
    if ((i - 1 < 0 || regex[i - 1] != '\\') && regex[i] == '-') {
      for (int j = regex[i - 1] + 1; j < regex[i + 1]; j++) {
        newRegex.push_back(j);

        if (j != regex[i + 1] - 1)
          newRegex.push_back('|');
      }
    }
    else {
      newRegex.push_back(regex[i]);
    }
  }

  return newRegex;
}

static string set(string regex) {
  stack<int> stack;
  string newRegex;
  int n = regex.length();

  for (int i = 0; i < n; i++) {
    if ((i == 0 || regex[i - 1] != '\\') && regex[i] == '[') {
      newRegex.push_back('(');
      stack.push(i);
    }
    else if ((i == 0 || regex[i - 1] != '\\') && regex[i] == ']') {
      newRegex.push_back(')');
      stack.pop();

      if (!stack.empty() &&  i + 1 < n && regex[i + 1] != ')')
        newRegex.push_back('|');
    }
    else {
      newRegex.push_back(regex[i]);

      if (!stack.empty() && i + 1 < n)
        if ((i == 0 || regex[i - 1] != '\\') && regex[i] != '\\' && regex[i] != '(' && regex[i + 1] != ')' && regex[i] != '|' && regex[i + 1] != '|' && regex[i + 1] != ']')
          newRegex.push_back('|');
    }
  }

  return newRegex;
}

static string oneOrMore(string regex) {
  stack<int> stack; // parentesis position in the new regex string
  string newRegex;

  for (int i = 0; i < (int) regex.length(); i++) {
    if ((i - 1 < 0 || regex[i - 1] != '\\') && regex[i] == '+') {
      if (newRegex[newRegex.length() - 2] != '\\' && newRegex[newRegex.length() - 1] != ')') {
        // is a simple character
        newRegex.push_back(newRegex[newRegex.length() - 1]);
        newRegex.push_back('*');
      }
      else {
        int j = newRegex.length() - 1;
        string aux;

        while (j >= 0 && stack.top() != j) {
          aux.push_back(newRegex[j]);
          j -= 1;
        }

        if (j >= 0) {
          aux.push_back(newRegex[j]);
          stack.pop();

          for (int k = aux.length() - 1; k >= 0; k--)
            newRegex += aux[k];
          
          newRegex += '*';
        }
      }
    }
    else {
      if (newRegex[newRegex.length() - 1] != '\\' && regex[i] == '(')
        stack.push(newRegex.length());
      
      newRegex.push_back(regex[i]);
    }
  }

  return newRegex;
}

static string complement(string regex) {
  string newRegex;
  int n = regex.length();

  for (int i = 0; i < n; i++) {
    if ((i == 0 || regex[i - 1] != '\\') && regex[i] == '^') {
      vector<bool> myMap(128, false);

      for (int j = 33; j < 127; j++)
        myMap[j] = false;

      cout << "LETRAS: " << regex[i + 1] << endl;
      cout << "LETRAS: " << regex[i + 2] << endl;
      cout << "LETRAS: " << regex[i + 3] << endl; 

      if (regex[i + 1] != '\\' && regex[i + 2] == '-') {
        for (int j = regex[i + 1]; j <= regex[i + 3]; j++) {
          myMap[j] = true;
        }

        i += 3;
      }
      else {
        for (int j = regex[i + 1]; j != ']'; j = regex[i + 1]) {
          myMap[j] = true;
          i += 1;
        }
      }
      
      for (int j = 33; j < 127; j++) {
        if (!myMap[j]) {
          if (j == '(' || j == ')' || j == '[' || j == ']' || 
              j == '*' || j == '|' || j == '+' || j == '-' || 
              j == '\\' || j == '^' || j == '.')
              newRegex.push_back('\\');

          newRegex.push_back(j);

          if (j < 126)
            newRegex.push_back('|');
        }
        else {
          if (j == 126)
            newRegex.erase(newRegex.length() - 1, 1);
        }
      }
    }
    else {
      newRegex.push_back(regex[i]);
    }
  }

  return newRegex;
}
