#include "Graph.hpp"
#include <iostream>
using namespace std;

Graph::Graph(int vertices) {
  V = vertices;
  graph = vector<vector<int>>(V + 1);
}

Graph::~Graph() {
  
}

int Graph::vertices() {
  return V;
} 

void Graph::addArrow(int u, int v) {
  graph[u].push_back(v);
}

void Graph::dfs(int v, vector<bool> & marked) {
  marked[v] = true;

  for (int i = 0; i < (int) graph[v].size(); i++) {
    int u = graph[v].at(i);

    if (!marked[u])
      dfs(u, marked);
  }
}

void Graph::showGraph() {
  for (int v = 0; v < V; v++) {
    cout << "Vertex: " << v << endl;

    for (int i = 0; i < (int) graph[v].size(); i++)
      cout << graph[v].at(i) << " ";

    cout << endl;
  }
}
